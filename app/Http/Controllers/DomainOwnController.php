<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DomainOwnController extends Controller
{
    //

    public function index(Request $request){

        return view('site.pages.success-domain')->with([
            "fname" => $request->fname,
            "lname" => $request->lname,
            "agentid" => $request->agentid,
            "auth" => $request->auth,
            "email" => $request->email,
            "prtname" => $request->prtname,
            "cea"=> $request->cea,
            "type" => $request->type,
            "staging"=> $request->staging,
            "ip" => $request->ip,
            "siteid"=> $request->siteid,
            "status"=> true,
            'message'=> '',
            "isRenew" => $request->isRenew
        ]);
    }
    public function pingDomain(Request $request){

        $host = $request->staging == 1 ? env('PROPNEX_IP') : env('PROPNEX_IP_PROD');
        $url = $host. '/propnex/index.php/domain/Ping_domain?domain='.urlencode($request->domain).'&host_ip='.$request->ip;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch,CURLOPT_HTTPHEADER,[
            'Content-type: application/json'
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = json_decode(curl_exec($ch));
        if(!is_null($response)){
            if($response->status == 0){
        
                $host = $request->staging == 1 ? env('PROPNEX_IP') : env('PROPNEX_IP_PROD');
                $url = $host . '/propnex/index.php/pnapi/addDomain?type=own&domain='.$request->domain. '&site-id='. $request->siteid;
        
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url );
                curl_setopt($ch,CURLOPT_HTTPHEADER,[
                    'Authentication: '.$request->agentid . ':'. $request->auth
                ]);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $response = json_decode(curl_exec($ch));
                curl_close($ch);
        
                if($response->status == "ok"){
                    return redirect()->route('ping-domain-success',[
                        "fname" => $request->fname,
                        "lname" => $request->lname,
                        "agentid" => $request->agenatid,
                        "auth" => $request->auth,
                        "email" => $request->email,
                        "prtname" => $request->prtname,
                        "cea"=> $request->cea,
                        "type" => $request->type,
                        "staging"=> $request->staging,
                        "domain" => $request->domain,
                        'status' => true,
                        "ip" => $request->ip,
                        "siteid"=> $request->siteid,
                        "isRenew" => $request->isRenew
                    ]);
                }else{
                    return redirect()->route('ping-domain-fail',[
                        "fname" => $request->fname,
                        "lname" => $request->lname,
                        "agentid" => $request->agenatid,
                        "auth" => $request->auth,
                        "email" => $request->email,
                        "prtname" => $request->prtname,
                        "cea"=> $request->cea,
                        "type" => $request->type,
                        "staging"=> $request->staging,
                        "domain" => $request->domain,
                        'status' => true,
                        "ip" => $request->ip,
                        "siteid"=> $request->siteid,
                        "isRenew" => $request->isRenew
                    ]);
                }
        
        
            }else{
                return redirect()->route('ping-domain-fail',[
                    "fname" => $request->fname,
                    "lname" => $request->lname,
                    "agentid" => $request->agenatid,
                    "auth" => $request->auth,
                    "email" => $request->email,
                    "prtname" => $request->prtname,
                    "cea"=> $request->cea,
                    "type" => $request->type,
                    "staging"=> $request->staging,
                    "domain" => $request->domain,
                    'status' => true,
                    "ip" => $request->ip,
                    "siteid"=> $request->siteid,
                    "isRenew" => $request->isRenew
                ]);
            }
        }
        else{
            return redirect()->route('ping-domain-fail',[
                    "fname" => $request->fname,
                    "lname" => $request->lname,
                    "agentid" => $request->agenatid,
                    "auth" => $request->auth,
                    "email" => $request->email,
                    "prtname" => $request->prtname,
                    "cea"=> $request->cea,
                    "type" => $request->type,
                    "staging"=> $request->staging,
                    "domain" => $request->domain,
                    'status' => true,
                    "ip" => $request->ip,
                    "siteid"=> $request->siteid,
                    "isRenew" => $request->isRenew
                ]);
            }
        }

    public function pingDomainfail(Request $request){
        return view('site.pages.ping-domain-fail')->with([
            "fname" => $request->fname,
            "lname" => $request->lname,
            "agentid" => $request->agentid,
            "auth" => $request->auth,
            "email" => $request->email,
            "prtname" => $request->prtname,
            "cea"=> $request->cea,
            "type" => $request->type,
            "staging"=> $request->staging,
            "domain" => $request->domain,
            'status' => false,
            "ip" => $request->ip,
            "siteid"=> $request->siteid,
            "isRenew" => $request->isRenew
        ]);
    }

    public function pingDomainSuccess(Request $request){
        return view('site.pages.ping-domain-success')->with([
            "fname" => $request->fname,
            "lname" => $request->lname,
            "agentid" => $request->agentid,
            "auth" => $request->auth,
            "email" => $request->email,
            "prtname" => $request->prtname,
            "cea"=> $request->cea,
            "type" => $request->type,
            "staging"=> $request->staging,
            "domain" => $request->domain,
            'status' => false,
            "ip" => $request->ip,
            "siteid"=> $request->siteid,
            "isRenew" => $request->isRenew
        ]);
    }


    public function showInstructions(Request $request){
        return view('site.pages.instructions')->with([
            "fname" => $request->fname,
            "lname" => $request->lname,
            "agentid" => $request->agentid,
            "auth" => $request->auth,
            "email" => $request->email,
            "prtname" => $request->prtname,
            "cea"=> $request->cea,
            "type" => $request->type,
            "staging"=> $request->staging,
            "domain" => $request->domain,
            'status' => false,
            "ip" => $request->ip,
            "siteid"=> $request->siteid,
            "isRenew" => $request->isRenew,
            "message" => ""
        ]);
    }

}
