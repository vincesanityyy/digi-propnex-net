<?php

namespace App\Http\Controllers;

use App\Transactions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Mail\TransportManager;


class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $date = date("Y-m-d H:i:s");
        $transaction = new Transactions();
        $transaction->domain = $request->domain;
        $transaction->ip = $request->ip;
        $transaction->agentid = $request->agentid;
        $transaction->site_id = $request->site_id;
        $transaction->staging = $request->staging;
        $transaction->expire_date = $request->expire_date;
        $transaction->created_at = $date;
        $transaction->updated_at = $date;
        $transaction->save();

        return response()->json([
            "status" => "ok",
            "data" => $transaction
        ]);
    }

    public function getRegisterDomains(Request $request){

        $transaction = Transactions::where('agentid', $request->agentid)
            ->get();

        return response()->json([
           "status" => "ok",
            "data" => $transaction
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function show(Transactions $transactions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function edit(Transactions $transactions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transactions $transactions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $transaction = Transactions::where('id', $request->id)->first();
        $transaction->delete();

        return response()->json([
            "status" => "ok",
            "message" => "domain deleted"
        ]);

    }

    public function addBuyDomain(){

        $url = 'https://pa-staging.propnex.net/index.php/pnapi/addBuyDomain?agentid=27254&asdasdasdtest.tv&site-id=44&expire-date=2020-03-11';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch,CURLOPT_HTTPHEADER,[
            'Content-type: application/json'
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        return $response;

    }

}
