<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BuyDomainController extends Controller
{
    //
    public function index(Request $request){

     
            return view('site.pages.buy-domain')->with([
                "fname" => $request->fname,
                "lname" => $request->lname,
                "auth" => $request->auth,
                "agentid" => $request->agentid,
                "email" => $request->email,
                "prtname" => $request->prtname,
                "cea"=> $request->cea,
                "staging"=> $request->staging,
                "ip" => $request->ip,
                "siteid"=> $request->siteid,
                "isRenew" => $request->isRenew,
                "status" => true,
                'device' => $request->device,
            ]);
        
    }

    public function renewDomain(Request $request){

//        return response()->json($this->getDomainPrice($request));
        if(isset($this->getDomainPrice($request)->price)){
            return view('site.pages.paypal')->with([
                "fname" => $request->fname,
                "lname" => $request->lname,
                "auth" => $request->auth,
                "agentid" => $request->agentid,
                "email" => $request->email,
                "prtname" => $request->prtname,
                "cea"=> $request->cea,
                "staging"=> $request->staging,
                "ip" => $request->ip,
                "siteid"=> $request->siteid,
                "domain" => $request->domain,
                "price" => $this->getDomainPrice($request)->price,
                "status" => true,
                "message" => "",
                "isRenew" => $request->isRenew
                
            ]);
        }else{
            return view('site.pages.buy-domain')->with([
                "fname" => $request->fname,
                "lname" => $request->lname,
                "auth" => $request->auth,
                "agentid" => $request->agentid,
                "email" => $request->email,
                "prtname" => $request->prtname,
                "cea"=> $request->cea,
                "staging"=> $request->staging,
                "ip" => $request->ip,
                "siteid"=> $request->siteid,
                "domain" => $request->domain,
                "status" => false,
                "message" => "Unauthorized: Invalid Token",
                "isRenew" => $request->isRenew
            ]);
        }


    }

    private function getDomainPrice($request){

        $host = $request->staging == 1 ? env('PROPNEX_IP') : env('PROPNEX_IP_PROD');
        $url = $host. '/propnex/index.php/pnapi/getDomainPrice?domain='.$request->domain;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch,CURLOPT_HTTPHEADER,[
            'Authentication: '.$request->agentid . ':'. $request->auth
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);


        return json_decode($response);
    }
}




