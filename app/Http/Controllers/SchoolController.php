<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SchoolController extends Controller
{
    // public function searchSchool(Request $request){
    //     $result = \DB::table('scraping.xp_pn_schools')
    //                 ->where('mainlevel_code','LIKE','%'.$request->level.'%')
    //                 ->select('school_name','lat','lng')
    //                 ->get();
    //     foreach($result as $data){
    //         $data->school_name = ucwords(strtolower($data->school_name));
    //     }
    //     return $result;
    // }

    public function searchSchool(Request $request){
        if($request->schoolRank === 'primary'){
            if($request->sort === 'A-Z'){
                $result = \DB::table('scraping.xp_pn_schools')
                ->select('rank','school_name','lat','lng')
                ->where('mainlevel_code','PRIMARY')
                ->where('rank', '!=', 0)
                ->orderBy('school_name','ASC')
                ->get();

                foreach($result as $data){
                    $data->school_name = ucwords(strtolower($data->school_name));
                }
                return $result;
            }else{
                $result = \DB::table('scraping.xp_pn_schools')
                ->select('rank','school_name','lat','lng')
                ->where('mainlevel_code','PRIMARY')
                ->where('rank', '!=', 0)
                ->orderBy('rank','ASC')
                ->get();

                foreach($result as $data){
                    $data->school_name = ucwords(strtolower($data->school_name));
                }
                return $result;
            }
            }elseif($request->schoolRank === 'secondary'){
            if($request->sort === 'A-Z'){
                $result = \DB::table('scraping.xp_pn_schools')
                    ->select('rank','school_name','lat','lng')
                    ->where('mainlevel_code','SECONDARY')
                    ->where('rank', '!=', 0)
                    ->orderBy('school_name','ASC')
                    ->get();

                foreach($result as $data){
                    $data->school_name = ucwords(strtolower($data->school_name));
                }
                return $result;
            }else{
                $result = \DB::table('scraping.xp_pn_schools')
                ->select('rank','school_name','lat','lng')
                ->where('mainlevel_code','SECONDARY')
                ->where('rank', '!=', 0)
                ->orderBy('rank','ASC')
                ->get();

                foreach($result as $data){
                    $data->school_name = ucwords(strtolower($data->school_name));
                }
                return $result;
            }
            }else{
               if($request->sort === 'A-Z'){
                $result = \DB::table('scraping.xp_pn_schools')
                ->where('mainlevel_code','LIKE','%'.$request->level.'%')
                ->orderBy('school_name','ASC')
                ->select('school_name','lat','lng')
                ->get();
                foreach($result as $data){
                    $data->school_name = ucwords(strtolower($data->school_name));
                }
                return $result;
               }else{
                $result = \DB::table('scraping.xp_pn_schools')
                ->where('mainlevel_code','LIKE','%'.$request->level.'%')
                ->select('school_name','lat','lng')
                ->get();
                foreach($result as $data){
                    $data->school_name = ucwords(strtolower($data->school_name));
                }
                return $result;
               }
        }
    }
}
