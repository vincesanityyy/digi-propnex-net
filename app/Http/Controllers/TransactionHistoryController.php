<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TransactionHistoryController extends Controller
{
    public function getTransactionHistory(Request $request){
        $result = \DB::table('scraping.xp_guru_properties')
            ->where('property_name',$request->projectName)
            ->select('property_name')
            ->get();
        $result = json_decode($result,true);
        dd($result);
        $transact = \DB::table('xp_pn_ura_transactions')
            ->where('project_name','LIKE','%'.$result[0]['property_name'].'%')
            ->select('transtype','project_name','unitname','block','street','level','stack','no_of_units','area','type_of_area','transacted_price','nettprice','unitprice_psm','unitprice_psf','sale_date',
            'contract_date','property_type','tenure','completion_date','type_of_sale','purchaser_address_indicator','postal_district','postal_sector','postal_code','planning_region','planning_area')
            ->distinct()
            ->get();
       
        return $transact;
    }
}
