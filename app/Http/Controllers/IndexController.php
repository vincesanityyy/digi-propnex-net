<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    //
    public function index(Request $request){
        return view('site.pages.index')->with([
            "fname" => $request->fname,
            "lname" => $request->lname,
            "auth" => $request->auth,
            "agentid" => $request->agentid,
            "email" => $request->email,
            "prtname" => $request->prtname,
            "cea"=> $request->cea,
            "staging"=> $request->staging,
            "ip" => $request->ip,
            "siteid"=> $request->siteid,
            "isRenew" => $request->isRenew
        ]);
    }
}
