<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConfirmDomainController extends Controller
{
    //

    public function index(Request $request){
        return view('site.pages.confirm-domain')->with([
            "fname" => $request->fname,
            "lname" => $request->lname,
            "agentid" => $request->agentid,
            "auth" => $request->auth,
            "email" => $request->email,
            "prtname" => $request->prtname,
            "cea"=> $request->cea,
            "type" => $request->type,
            "staging"=> $request->staging,
            "ip" => $request->ip,
            "siteid"=> $request->siteid,
            "status"=> true,
            'message'=> '',
            "isRenew" => $request->isRenew
        ]);
    }

    public function verifyEmail(Request $request){

            $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
            $beautymail->send('emails.welcome', [],  function($message) use($request) 
             {
               
                $message
                    ->from('testemail.ideahub@gmail.com','Test Email')
                    ->to($request->email)
                    ->subject('Test Email');
            });

            return view('site.pages.verify-email')->with([
                "fname" => $request->fname,
                "lname" => $request->lname,
                "agentid" => $request->agentid,
                "auth" => $request->auth,
                "email" => $request->email,
                "prtname" => $request->prtname,
                "cea"=> $request->cea,
                "type" => $request->type,
                "staging"=> $request->staging,
                "ip" => $request->ip,
                "siteid"=> $request->siteid,
                "status"=> true,
                'message'=> '',
                "isRenew" => $request->isRenew
            ]);
    }

    public function getDomain(Request $request){
        $host = $request->staging == 1 ? '52.77.29.188' : '127.0.0.1';
        
        // $host = $request->staging == 1 ? env('PROPNEX_IP') : env('PROPNEX_IP_PROD');
        $url = $host. '/propnex/index.php/domain/Getdomain?domain='.urlencode($request->domain);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch,CURLOPT_HTTPHEADER,[
            'Content-type: application/json'
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = json_decode(curl_exec($ch));
        curl_close($ch);
        // $price = $this->getDomainPrice($response);
        // dd($url);
        if(preg_match("/^(?!:\/\/)(?=.{1,255}$)((.{1,63}\.){1,127}(?![0-9]*$)[a-z0-9-]+\.?)$/i", $request->domain) == 1){
           
            if (isset($this->getDomainPrice($request)->price)){
                
                return view('site.pages.confirm-domain')->with([
                    "fname" => $request->fname,
                    "lname" => $request->lname,
                    "agentid" => $request->agentid,
                    "auth" => $request->auth,
                    "email" => $request->email,
                    "prtname" => $request->prtname,
                    "type" => $request->type,
                    "domain" => $request->domain,
                    "cea"=> $request->cea,
                    "staging"=> $request->staging,
                    "ip" => $request->ip,
                    "siteid"=> $request->siteid,
                    "status" => false,
                    "message" => "Unauthorized: Invalid Token",
                    "isRenew" => $request->isRenew
                ]);
            } else if($response->status == "fail" ){
                return view('site.pages.confirm-fail')->with([
                    "fname" => $request->fname,
                    "lname" => $request->lname,
                    "agentid" => $request->agentid,
                    "auth" => $request->auth,
                    "email" => $request->email,
                    "prtname" => $request->prtname,
                    "type" => $request->type,
                    "domain" => $request->domain,
                    "cea"=> $request->cea,
                    "staging"=> $request->staging,
                    "ip" => $request->ip,
                    "siteid"=> $request->siteid,
                    "isRenew" => $request->isRenew
                ]);

            }else{
                return view('site.pages.success-domain')->with([
                    "fname" => $request->fname,
                    "lname" => $request->lname,
                    "auth" => $request->auth,
                    "agentid" => $request->agentid,
                    "email" => $request->email,
                    "prtname" => $request->prtname,
                    "type" => $request->type,
                    "domain" => $request->domain,
                    "price" => '97',
                    "cea"=> $request->cea,
                    "staging"=> $request->staging,
                    "ip" => $request->ip,
                    "siteid"=> $request->siteid,
                    "isRenew" => $request->isRenew
                ]);

            }
        }else{
            return view('site.pages.confirm-domain')->with([
                "fname" => $request->fname,
                "lname" => $request->lname,
                "agentid" => $request->agentid,
                "auth" => $request->auth,
                "email" => $request->email,
                "prtname" => $request->prtname,
                "type" => $request->type,
                "domain" => $request->domain,
                "cea"=> $request->cea,
                "staging"=> $request->staging,
                "ip" => $request->ip,
                "siteid"=> $request->siteid,
                "status" => false,
                "message" => "Invalid Domain Name",
                "isRenew" => $request->isRenew
            ]);
        }

        // dd($request->domain);


    }

    private function getDomainPrice($request){

        // $host = $request->staging == 1 ? env('PROPNEX_IP') : env('PROPNEX_IP_PROD');
        $host = $request->staging == 1 ? '52.77.29.188' : '127.0.0.1';
        $url = $host. '/propnex/index.php/pnapi/getDomainPrice?domain='.$request->domain;

//        return 'Authentication: '.$request->agentid . ':'. $request->auth;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch,CURLOPT_HTTPHEADER,[
           'Authentication: '.$request->agentid . ':'. $request->auth
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);


        return json_decode($response);
    }


    public function addOwnDomain(Request $request){

        $host = $request->staging == 1 ? env('PROPNEX_IP') : env('PROPNEX_IP_PROD');
        $url = $host . '/propnex/index.php/pnapi/addDomain?type=own&domain='.$request->domain. '&site-id='. $request->siteid;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch,CURLOPT_HTTPHEADER,[
            'Authentication: '.$request->agentid . ':'. $request->auth
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = json_decode(curl_exec($ch));
        curl_close($ch);


        if ($response->status == 'ok'){
            return redirect()->route('success-domain-own',[
                "fname" => $request->fname,
                "lname" => $request->lname,
                "agentid" => $request->agentid,
                "auth" => $request->auth,
                "email" => $request->email,
                "prtname" => $request->prtname,
                "cea"=> $request->cea,
                "type" => $request->type,
                "staging"=> $request->staging,
                "domain" => $request->domain,
                'status' => true,
                "ip" => $request->ip,
                "siteid"=> $request->siteid,
                "isRenew" => $request->isRenew
            ]);

        }else{
            $message = !isset($response->error)? 'Unauthorized: Invalid Token' : $response->error;
            return view('site.pages.confirm-domain')->with([
                "fname" => $request->fname,
                "lname" => $request->lname,
                "agentid" => $request->agentid,
                "auth" => $request->auth,
                "email" => $request->email,
                "prtname" => $request->prtname,
                "cea"=> $request->cea,
                "type" => $request->type,
                "staging"=> $request->staging,
                "domain" => $request->domain,
                'status' => false,
                "ip" => $request->ip,
                "siteid"=> $request->siteid,
                'message' => $message,
                "isRenew" => $request->isRenew
            ]);
        }
    }

    public function successDomainOwn(Request $request){

        return view('site.pages.success-domain-own')->with([
            "fname" => $request->fname,
            "lname" => $request->lname,
            "agentid" => $request->agentid,
            "auth" => $request->auth,
            "email" => $request->email,
            "prtname" => $request->prtname,
            "cea"=> $request->cea,
            "type" => $request->type,
            "staging"=> $request->staging,
            "domain" => $request->domain,
            'status' => false,
            "ip" => $request->ip,
            "siteid"=> $request->siteid,
            "isRenew" => $request->isRenew
        ]);
    }
}
