<?php

class DomainController extends Controller
{

    public function PointWebnic()
    {
        $link = null;
        if ($_SERVER["HTTP_HOST"] == "52.77.29.188") {
            $link = "https://ote.webnic.cc/";
        } else {
            $link = "https://my.webnic.cc/";
            //$link = "https://my.webnic.cc/";
        }

        return $link;
    }

    public function actionGetdomain()
    {
        //post method : parameter -> domain
        $domain = $_REQUEST['domain'];
        //$param variable to setOption for source and domain
        $param = array(
            'source' => 'webcc-propnex123',
            'domain' => $domain
        );
        //to get the result it will  search $param by given api url
        //get(url,search perameter)
        // get response and store in $output
        $PointWebnic = $this->PointWebnic();

        $response = Yii::app()->curl->get($PointWebnic . "jsp/pn_qry.jsp", $param);


        $code = $response[0];
        $response = substr($response, 2);
        if ($code == 0) {
            $status = "ok";
            $message = $response;
        } else {
            $status = "fail";
            $message = $response;
        }
        $response_json = array(
            'status' => $status,
            'message' => $message
        );
        echo json_encode($response_json);
    }

    /*
        Request URL: http://phpstack-112500-793563.cloudwaysapps.com/API/index.php/api/Renewal_domain
        this is Renewal domain name  function which is renew the domain
            Required parameter:
                             1: Domain Name
                             2:Term {number of year}
            Response : if you gave renewed{"status":"fail","message":"message (Domain has just been renewed)\n"}
            otherwise it will return new expiry date with status code ok

        */

    function actionRenewal_domain()
    {

        $date = date("Y:m:d H:i:s");
        $cehecksum = md5("webcc-propnex123" . $date . md5("3Zj6F3sza9M3rnPu"));
        $param = array(
            'source' => 'webcc-propnex123',
            'domainname' => $_GET['domainname'],
            'otime' => $date,
            'ochecksum' => $cehecksum,
            'term' => $_GET['term']
        );
        $PointWebnic = $this->PointWebnic();
        $response = Yii::app()->curl->get($PointWebnic . 'jsp/pn_renew.jsp', $param);

        $_REQUEST["status"] = "renew";
        $code = $response[0];
        $response = substr($response, 2);
        if ($code == 0) {
            $status = "ok";
            $message = $response;
            $this->actionCreateTransaction($_REQUEST);
        } else {
            $status = "fail";
            $message = $response;
        }
        $response_json = array(
            'status' => $status,
            'message' => $message
        );

        echo json_encode($response_json);
    }

    /*
    Request URL: http://phpstack-112500-793563.cloudwaysapps.com/API/index.php/api/cname
    this is the cname (conical name) function it will use to Aliases URL in url formate
    require  parameter :
            1. domain name
            2. at least one parameter require to updae or make cname {c,cs}
                "c" => 'c'  Aliases URL, in url format, example : www1 or ftp1.
                "cs" =>'cs' sub domain, example "www","marketing","sales" etc.
            3. response: {"status":"ok","message":"Command Completed Successfully (CNAME Record updated successfully)\n"}

    */
    function actionCname()
    {

        $date = date("y:m:d H:i:s");
        $cehecksum = md5('webcc-propnex123' . $date . md5('3Zj6F3sza9M3rnPu'));

        $param = array(
            'source' => 'webcc-propnex123',
            'domain' => $_GET['domain'],
            'otime' => $date,
            'ochecksum' => $cehecksum,
            'action' => 'cnrecord',
            'c' => $_GET['c'],
            'cs' => $_GET['cs'],
        );
        $PointWebnic = $this->PointWebnic();
        $response = Yii::app()->curl->get($PointWebnic . 'jsp/pn_valuesadd.jsp', $param);

        $code = $response[0];
        $response = substr($response, 2);
        if ($code == 0) {
            $status = "ok";
            $message = "Command " . $response;
        } else {
            $status = "fail";
            $message = "Command " . $response;
        }
        $response_json = array(
            'status' => $status,
            'message' => $message
        );

        echo json_encode($response_json);
    }

    /*
        Request URL: http://phpstack-112500-793563.cloudwaysapps.com/API/index.php/api/arecord
        This is A recorde function it set ip address to sub domain
        require parameter:
                        1: domain name
                        2:	ip1  =>First A pointer values, in IP format.
                        3:	sub1 =>sub domain that points to ip1 is particularly set parameter for "yourdomain.com". Any parameter passing in this field,it always consider it as "".

                            Response : {"status":"ok","message":"Command Completed Successfully (A pointer updated successfully)\n"}
    */
    function actionArecord()
    {

        $date = date("y:m:d H:i:s");
        $cehecksum = md5('webcc-propnex123' . $date . md5('3Zj6F3sza9M3rnPu'));
        $param = array(
            'source' => 'webcc-propnex123',
            'domain' => $_GET['domain'],
            'otime' => $date,
            'ochecksum' => $cehecksum,
            'action' => 'apointer',
            'ip1' => $_GET['ip1'],
            'ip2' => $_GET['ip1'],
            'sub1' => $_GET['sub1'],
            'sub2' => 'www'

        );
        $PointWebnic = $this->PointWebnic();
        $response = Yii::app()->curl->get($PointWebnic . 'jsp/pn_valuesadd.jsp', $param);

        $code = $response[0];
        $response = substr($response, 2);
        if ($code == 0) {
            $status = "ok";
            $message = "Command " . $response;
        } else {
            $status = "fail";
            $message = "Command " . $response;
        }
        $response_json = array(
            'status' => $status,
            'message' => $message
        );

        echo json_encode($response_json);
    }

    /*
        Request URL: http://phpstack-112500-793563.cloudwaysapps.com/API/index.php/api/Domain_registration
        Request URL with params:  http://phpstack-112500-793563.cloudwaysapps.com/API/index.php/api/Domain_registration?domainname=devdomaintest.tv&ns1=ns1.web.cc&ns2=ns2.web.cc&newuser=test&reg_company=Domain owner&reg_fname=jaylord&reg_lname=lagud&reg_email=jaylord.lagud@gmail.com&reg_telephone=+123.1234567890&reg_contact_type=0&reg_country=PH&reg_state=Davao Del sur&reg_city=Davao&reg_addr1=Tigatto&reg_postcode=8000

        this is Renewal domain name  function which is renew the domain
            Required parameter:
                             1:domainname
                             2:term {number of year}
                             3:ns1 {for testing - ns1.web.cc}
                             4:ns2 {for testing - ns2.web.cc}
                             5:newuser {username}
                             6:reg_company
                             7:reg_fname
                             8:reg_lname
                             9:reg_email
                             10:reg_telephone { sample: +123.1234567890}
                             11:reg_contact_type {0}
                             12:reg_country {PH or SG etc.}

            Response : if you gave renewed{"status":"fail","message":"message (Domain has just been renewed)\n"}
            otherwise it will return new expiry date with status code ok
        */


    function actionDomain_registration()
    {


        $date = date("Y-m-d H:i:s");
        $cehecksum = md5("webcc-propnex123" . $date . md5('3Zj6F3sza9M3rnPu'));
        //$_GET['reg_telephone'] = urldecode($_GET['reg_telephone']);
        $param = [
            'reg_contact_type' => 1,
            'proxy' => $_GET['proxy'],
            'custom_reg1' => '199903004H',
            'source' => 'webcc-propnex123',
            'username' => 'propnex123',
            'password' => 'ALUkyxdKvJU5$XC',
            'domainname' => $_GET['domainname'],
            'otime' => $date,
            'ochecksum' => $cehecksum,
            'term' => $_GET['term'],
            'ns1' => $_GET['ns1'],//ns1.web.cc
            'ns2' => $_GET['ns2'],//ns2.web.cc
            'reg_company' => $_GET['reg_company'],
            'reg_fname' => $_GET['reg_fname'],
            'reg_lname' => $_GET['reg_lname'],
            'reg_addr1' => $_GET['reg_addr1'],
            'reg_state' => $_GET['reg_state'],
            'reg_city' => $_GET['reg_city'],
            'reg_postcode' => $_GET['reg_postcode'],
            'reg_telephone' => $_GET['reg_telephone'],// %2B886.2579999
            'reg_country' => $_GET['reg_country'],
            'reg_email' => $_GET['reg_email'],
            'adm_contact_type' => 1,//$_GET['adm_contact_type'],
            'adm_company' => 'Propnex Realty Pte Ltd',
            'adm_fname' => 'Michael',
            'adm_lname' => 'Koh',
            'adm_addr1' => '480 Lorong 6 Toa Payoh',
            'adm_state' => 'sg',
            'adm_city' => 'Singapore',
            'adm_postcode' => '310480',
            'adm_country' => 'sg',
            'adm_email' => 'michael.koh@propnex.com',
            'custom_adm1' => "199903004H",
            'adm_telephone' => '+603.89966788',//%2B886.2579999
            'tec_company' => 'Propnex Realty Pte Ltd',
            'tec_fname' => 'Michael',
            'tec_lname' => 'Koh',
            'tec_email' => 'michael.koh@propnex.com',
            'tec_telephone' => '+603.89966788',//%2B886.2579999
            'tec_addr1' => '480 Lorong 6 Toa Payoh',
            'tec_postcode' => '310480',
            'tec_country' => 'sg',
            'tec_city' => 'Singapore',
            'tec_contact_type' => 1,
            'tec_state' => 'sg',
            'bil_company' => 'Propnex Realty Pte Ltd',
            'bil_fname' => 'Michael',
            'bil_lname' => 'Koh',
            'bil_city' => 'Singapore',
            'bil_state' => 'Singapore',
            'bil_email' => 'michael.koh@propnex.com',
            'bil_country' => 'sg',
            'bil_addr1' => '480 Lorong 6 Toa Payoh',
            'bil_telephone' => '+603.89966788',//%2B886.2579999
            'bil_postcode' => '310480',
            'bil_contact_type' => 1,//$_GET['bil_contact_type'],
            'newuser' => "old",
            'custom_tec1' => "199903004H",
            'custom_bil1' => "199903004H",
        ];
        $PointWebnic = $this->PointWebnic();
        $response = Yii::app()->curl->get($PointWebnic . 'jsp/pn_newreg.jsp', $param);
        // $array_response = explode(' ',$response,2);
        // 	  $status_code = $array_response[0];
        // 	  $status_message = $array_response[1];
        $code = $response[0];

        if ($code == 0) {
            $status = "ok";
            $message = $response ? $response : "Registered Sucessfully!";
        } else {
            $status = "fail";
            $message = $response;
        }

        $response_json = array(
            'status' => $status,
            'message' => $message
        );
        echo json_encode($response_json);
    }


    function actionCustomDomain_registration()
    {

        $transaction_no = $_REQUEST['transaction_no'];
        $agentid = $_REQUEST['agentid'];
        $_REQUEST['status'] = 'new';

        $list = Yii::app()->db->createCommand("SELECT * FROM xp_pn_site_payments WHERE transaction_no='$transaction_no' AND agentid=$agentid")->queryAll();

        if ($list) {

            $date = date("Y-m-d H:i:s");
            $cehecksum = md5("webcc-propnex123" . $date . md5('3Zj6F3sza9M3rnPu'));
            //$_GET['reg_telephone'] = urldecode($_GET['reg_telephone']);
            $param = [
                'reg_contact_type' => 1,
                'proxy' => $_GET['proxy'],
                'custom_reg1' => '199903004H',
                'source' => 'webcc-propnex123',
                'username' => 'propnex123',
                'password' => 'ALUkyxdKvJU5$XC',
                'domainname' => $_GET['domainname'],
                'otime' => $date,
                'ochecksum' => $cehecksum,
                'term' => 1,
                'ns1' => $_GET['ns1'],//ns1.web.cc
                'ns2' => $_GET['ns2'],//ns2.web.cc
                'reg_company' => 'Propnex Realty Pte Ltd',
                'reg_fname' => $_GET['reg_fname'],
                'reg_lname' => $_GET['reg_lname'],
                'reg_addr1' => '480 Lorong 6 Toa Payoh',
                'reg_state' => 'sg',
                'reg_city' =>  'Singapore',
                'reg_postcode' => '310480',
                'reg_telephone' => '+603.89966788',// %2B886.2579999
                'reg_country' => 'sg',
                'reg_email' => $_GET['reg_email'],
                'adm_contact_type' => 1,//$_GET['adm_contact_type'],
                'adm_company' => 'Propnex Realty Pte Ltd',
                'adm_fname' => 'Michael',
                'adm_lname' => 'Koh',
                'adm_addr1' => '480 Lorong 6 Toa Payoh',
                'adm_state' => 'sg',
                'adm_city' => 'Singapore',
                'adm_postcode' => '310480',
                'adm_country' => 'sg',
                'adm_email' => 'michael.koh@propnex.com',
                'custom_adm1' => "199903004H",
                'adm_telephone' => '+603.89966788',//%2B886.2579999
                'tec_company' => 'Propnex Realty Pte Ltd',
                'tec_fname' => 'Michael',
                'tec_lname' => 'Koh',
                'tec_email' => 'michael.koh@propnex.com',
                'tec_telephone' => '+603.89966788',//%2B886.2579999
                'tec_addr1' => '480 Lorong 6 Toa Payoh',
                'tec_postcode' => '310480',
                'tec_country' => 'sg',
                'tec_city' => 'Singapore',
                'tec_contact_type' => 1,
                'tec_state' => 'sg',
                'bil_company' => 'Propnex Realty Pte Ltd',
                'bil_fname' => 'Michael',
                'bil_lname' => 'Koh',
                'bil_city' => 'Singapore',
                'bil_state' => 'Singapore',
                'bil_email' => 'michael.koh@propnex.com',
                'bil_country' => 'sg',
                'bil_addr1' => '480 Lorong 6 Toa Payoh',
                'bil_telephone' => '+603.89966788',//%2B886.2579999
                'bil_postcode' => '310480',
                'bil_contact_type' => 1,//$_GET['bil_contact_type'],
                'newuser' => "old",
                'custom_tec1' => "199903004H",
                'custom_bil1' => "199903004H",
            ];
            $PointWebnic = $this->PointWebnic();
            $response = Yii::app()->curl->get($PointWebnic . 'jsp/pn_newreg.jsp', $param);
            // $array_response = explode(' ',$response,2);
            // 	  $status_code = $array_response[0];
            // 	  $status_message = $array_response[1];
            $code = $response[0];

            if ($code == 0) {
                $status = "ok";
                $message = $response ? $response : "Registered Sucessfully!";
                $this->actionCreateTransaction($_REQUEST);

            } else {
                $status = "fail";
                $message = $response;
            }

        } else {
            $status = "fail";
            $message = 'Transaction no. does not exist.';
        }

        $response_json = array(
            'status' => $status,
            'message' => $message
        );

        echo json_encode($response_json);
    }


    public function actionCreateTransaction()
    {

        $status = $_REQUEST['status'];
        $transaction_no = $_REQUEST['transaction_no'];
        $agentid = $_REQUEST['agentid'];
        $pay_for = $_REQUEST['pay_for'];

        if ($status == 'paid') {
            Yii::app()->db->createCommand()
                ->insert(
                    'xp_pn_site_payments',
                    array(
                        'transaction_no' => $transaction_no,
                        'agentid' => $agentid,
                        'status' => $status,
                        'pay_for' => $pay_for
                    )
                );
            $response_json = array(
                'status' => 'ok',
                'message' => 'Success'
            );

            echo json_encode($response_json);
        } else {
            Yii::app()->db->createCommand("UPDATE xp_pn_site_payments set status='$status' WHERE transaction_no='$transaction_no' AND agentid=$agentid")->execute();
        }
    }

    /*
        Request URL: http://52.77.29.188/propnex/index.php/domain/Ping_domain?domain=propnex.net&host_ip=3.0.87.74
        this is Renewal domain name  function which is renew the domain
            Required parameter:
                             1: Domain Name

            Response : {"status":0,"message":"Domain connected."}
        */

    public function actionPing_domain()
    {

        $host_ip = $_GET['host_ip'];
        $domain = $_GET['domain'];

        $parse = parse_url($domain);

        if (!isset($parse['host'])):
            $url = $parse['path'];
        else:

            $url = $parse['host'];
        endif;
        $url = preg_replace('/^www\./i', '', $url);


        $domain_ip = gethostbyname($url);
        if ($domain_ip == $host_ip) {
            $status = 0;
            $message = "Domain connected.";
        } else {
            $status = 1;
            $message = "Domain is not connected to host ip address.";
        }

        $response_json = array(
            'status' => $status,
            'message' => $message
        );

        echo json_encode($response_json);
    }

    public function actionGuruScrapeData()
    {
        set_time_limit(0);
        $this->scrapeCondoCommercialDirectory();
        $this->scrapeHDBDirectory();
        $response_json = array(
            'status' => 'OK',
            'message' => 'Scraping Successfully'
        );
        echo json_encode($response_json);
    }

    public function scrapeHDBDirectory(){
        //Ang Mo Kio
        $values = $this->mergeHDBNameAndPostal(array(
                '1','3','6','10','4','8','2','5','9'
            ),'Ang Mo Kio Avenue');
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
                '11','22','51','12','31','52','21','32'
            ),'Ang Mo Kio Street');
        $this->scrapeData($values,'HDB');

        $values = array(
            "Bukit Batok Central"
        );
        $this->scrapeData($values,'HDB');
        //End Ang Mo Kio

        //Bukit Batok
        $values = $this->mergeHDBNameAndPostal(array(
                '5','3','6','4'
            ),'Bukit Batok Central');
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
                '21','25', '33', '52', '22', '31', '34', '11', '24', '32', '51'
            ),'Bukit Batok Street');
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            '5', '8', '2', '6', '9', '4', '7'
        ),'Bukit Batok West Avenue');
        $this->scrapeData($values,'HDB');
        //Bukit Batok

        //Bukit Timah
        $values = array(
            "Farrer Road",
            "Queen's Road",
            "Toh Yi Drive"
        );
        $this->scrapeData($values,'HDB');
        //End Bukit Timah

        //Clementi
        $values = $this->mergeHDBNameAndPostal(array(
            '2', '5', '12', '3', '6', '13', '4', '11', '14'
        ),'Clementi Avenue');
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            '2', '5', '12', '3', '6', '13', '4', '11', '14'
        ),'Clementi Avenue');
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            '12', '13', '11', '14'
        ),'Clementi Avenue');
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            '1', '2'
        ),'Clementi West Street');
        $this->scrapeData($values,'HDB');

        $values = array(
            "Commonwealth Avenue West",
            "Dover Crescent",
            "West Coast Drive",
            "West Coast Road",
        );
        $this->scrapeData($values,'HDB');
        //End Clementi

        //Jurong East
        $values = $this->mergeHDBNameAndPostal(array('1'),'Jurong East Avenue');
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            '13','21','24','31','32'
        ),'Jurong East Street');
        $this->scrapeData($values,'HDB');

        $values = array(
            "Pandan Gardens",
            "Teban Gardens Road",
            "Toh Guan Road",
            "Yuan Ching Road"
        );
        $this->scrapeData($values,'HDB');
        //End Jurong East

        //Marine Parade
        $values = array(
            "East Coast Road",
            "Marine Drive",
            "Joo Chiat Road",
            "Marine Parade Central",
            "Marine Crescent",
            "Marine Terrace"
        );
        $this->scrapeData($values,'HDB');
        //End Marine Parade

        //Queenstown
        $values = array(
            'Clarence Lane',
            'Commonwealth Crescent',
            'Dover Close East',
            'Empress Road',
            'Empress Road',
            'Holland Avenue',
            'Holland Avenue',
            'Kay Siang Road',
            'Margaret Drive',
            "Queen's Close",
            "Stirling Road",
            "Strathmore Avenue",
            "Commonwealth Avenue",
            "Commonwealth Drive",
            "Dover Crescent",
            "Ghim Moh Link",
            "Holland Close",
            "Kim Tian Road",
            "Mei Chin Road",
            "Tanglin Halt Road",
            "Commonwealth Close",
            "Dawson Road",
            "Dover Road",
            "Ghim Moh Road",
            "Holland Drive",
            "King's Road",
            "Mei Ling Street",
            "Ridout Road",
            "Tiong Bahru Road"
        );
        $this->scrapeData($values,'HDB');
        //End Queenstown

        //Serangoon
        $values = array(
            "Lorong Lew Lian",
            "Ripley Cresent",
            "Serangoon Central",
            "Serangoon Central Drive",
            "Upper Serangoon View"
        );
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "1","2","3","4"
        ),"Serangoon Avenue");
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "1","2","3","4"
        ),"Serangoon North Avenue");
        $this->scrapeData($values,'HDB');
        //End Serangoon

        //Woodlands
        $values = array(
            "Marsiling Crescent",
            "Marsiling Drive",
            "Marsiling Lane",
            "Marsiling Rise",
            "Marsiling Road",
            "South Woodlands Drive",
            "Woodlands Centre Road",
            "Woodlands Circle",
            "Woodlands Crescent",
            "Woodlands Ring Road",
            "Woodlands Rise"
        );
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "1","3","4","5","6","9"
        ),"Woodlands Avenue");
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "14","16","40","42","44","50","52","53","60","62","70","71","72","73","75"
        ),"Woodlands Drive");
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "11","13","31","32","41","81","82","83"
        ),"Woodlands Street");
        $this->scrapeData($values,'HDB');
        //End Woodlands

        //Bedok
        $values = array(
            "Bedok Central",
            "Bedok North Road",
            "Bedok Reservoir Crescent",
            "Bedok Reservoir Road",
            "Bedok Reservoir View",
            "Bedok South Road",
            "Chai Chee Avenue",
            "Chai Chee Drive",
            "Chai Chee Road",
            "Chai Chee Street",
            "Jalan Damai",
            "Jalan Tenaga",
            "Kew Drive",
            "Kew Terrace",
            "New Upper Changi Road"
        );
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "1","2","3","4"
        ),"Bedok North Avenue");
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "1","2","3","4"
        ),"Bedok North Street");
        $this->scrapeData($values,'HDB');
        //End Bedok

        //Bukit Merah
        $values = array(
            "Beo Crescent",
            "Boon Tiong Road",
            "Bukit Merah Central",
            "Bukit Merah Lane 1",
            "Bukit Merah View",
            "Bukit Purmei Road",
            "Chay Yan Street",
            "Chin Swee Road",
            "Delta Avenue",
            "Depot Road",
            "Eng Hoon Street",
            "Eng Watt Street",
            "Guan Chuan Street",
            "Havelock Road",
            "Henderson Crescent",
            "Henderson Road",
            "Hoy Fatt Road",
            "Indus Road",
            "Jalan Bukit Ho Swee",
            "Jalan Bukit Merah",
            "Jalan Klinik",
            "Jalan Kukoh",
            "Jalan Membina",
            "Jalan Minyak",
            "Jalan Rumah Tinggi",
            "Kampong Bahru Hill",
            "Kampong Bahru Road",
            "Kim Cheng Street",
            "Kim Pong Road",
            "Kim Tian Place",
            "Kim Tian Road",
            "Lengkok Bahru",
            "Lim Liak Street",
            "Lower Delta Road",
            "Moh Guan Terrace",
            "Redhill Close",
            "Redhill Lane",
            "Redhill Road",
            "Seng Poh Lane",
            "Seng Poh Road",
            "Silat Avenue",
            "Silat Road",
            "Silat Walk",
            "Taman Ho Swee",
            "Telok Blangah Crescent",
            "Telok Blangah Drive",
            "Telok Blangah Heights",
            "Telok Blangah Rise",
            "Telok Blangah Way",
            "Tiong Bahru Road",
            "Tiong Poh Road",
            "York Hill",
            "Zion Road"
        );
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "31","32"
        ),"Telok Blangah Street");
        $this->scrapeData($values,'HDB');
        //End Bukit Merah

        //Central Area
        $values = array(
            "Bain Street",
            "Buffalo Road",
            "Chander Road",
            "Devonshire Road",
            "Jalan Berseh",
            "Kelantan Lane",
            "Kitchener Road",
            "Maude Road",
            "Park Crescent",
            "Race Course Road",
            "Rowell Road",
            "Smith Street",
            "Tessensohn Road",
            "Waterloo Street",
            "Banda Street",
            "Cambridge Road",
            "Chin Swee Road",
            "Everton Park",
            "Jalan Sultan",
            "Kelantan Road",
            "Klang Lane",
            "New Market Road",
            "Perumal Road",
            "Rangoon Road",
            "Sago Lane",
            "Spottiswoode Park Road",
            "Upper Cross Street",
            "Beach Road",
            "Cantonment Close",
            "Crawford Lane",
            "French Road",
            "Jellicoe Road",
            "King George's Avenue",
            "Kreta Ayer Road",
            "North Bridge Road",
            "Queen Street",
            "Rochor Road",
            "Selegie Road",
            "Tanjong Pagar Plaza",
            "Veerasamy Road"
        );
        $this->scrapeData($values,'HDB');
        //End Central Area

        //Geylang
        $values = array(
            "Aljunied Avenue 2",
            "Aljunied Crescent",
            "Aljunied Road",
            "Balam Road",
            "Cassia Crescent",
            "Circuit Road",
            "Dakota Close",
            "Dakota Crescent",
            "Eunos Crescent",
            "Eunos Road 5",
            "Geylang East Avenue 1",
            "Geylang East Avenue 2",
            "Geylang East Central",
            "Geylang Serai",
            "Haig Road",
            "Jalan Batu",
            "Jalan Dua",
            "Jalan Enam",
            "Jalan Satu",
            "Jalan Tiga",
            "Joo Chiat Road",
            "Kallang Bahru",
            "Kampong Arang Road",
            "Kampong Kayu Road",
            "Lorong 3 Geylang",
            "Lorong 33 Geylang",
            "Macpherson Lane",
            "Merpati Road",
            "Old Airport Road",
            "Paya Lebar Way",
            "Pine Close",
            "Pipit Road",
            "Sims Drive",
            "Sims Place",
            "Ubi Avenue 1",
            "Upper Aljunied Lane",
            "Upper Boon Keng Road"
        );
        $this->scrapeData($values,'HDB');
        //End Geylang

        //Jurong West
        $values = array(
            "Boon Lay Avenue",
            "Boon Lay Drive",
            "Boon Lay Place",
            "Corporation Drive",
            "Ho Ching Road",
            "Hu Ching Road",
            "Kang Ching Road",
            "Tah Ching Road",
            "Tao Ching Road",
            "Yuan Ching Road",
            "Yung An Road",
            "Yung Ho Road",
            "Yung Kuang Road",
            "Yung Loh Road",
            "Yung Ping Road",
            "Yung Sheng Road",
        );
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(
            array(
                "1","3", "5"
            ),"Jurong West Avenue");
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(
            array(
                "1","3"
            ),"Jurong West Central");
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(
            array(
                "24","25","41","42","51","52","61","62","64","65","71","72","73","74","75","81","91","92","93"
            ),"Jurong West Street");
        $this->scrapeData($values,'HDB');
        //End Jurong West

        //Pasir Ris
        $values = array(
            "Changi Village Road",
            "Elias Road"
        );
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "1","10","3","4","6"
        ),"Pasir Ris Drive");
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "11","12","13","21","41","51","52","53","71","72"
        ),"Pasir Ris Street");
        $this->scrapeData($values,'HDB');
        //End Pasir Ris

        //Sembawang
        $values = array(
            "Admiralty Drive",
            "Admiralty Link",
            "Canberra Crescent",
            "Canberra Link",
            "Canberra Road",
            "Canberra Street",
            "Canberra Walk",
            "Montreal Drive",
            "Montreal Link",
            "Sembawang Close",
            "Sembawang Crescent",
            "Sembawang Drive",
            "SEMBAWANG DRIVE",
            "Sembawang Vista",
            "Sembawang Way",
            "Wellington Circle"
        );
        $this->scrapeData($values,'HDB');
        //End Sembawang

        //Tampines
        $values = array(
            "Simei Lane",
            "Simei Road",
        );
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "1", "2", "3","4","5"
        ),"Simei Street");
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "1","4","5", "7","8","9"
        ),"Tampines Avenue");
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "1","7","8"
        ),"Tampines Central");
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "11","12","21","22","23","24","32","33","34","41","42","43","44","45","71","72","81","82","83","84","86","91","92"
        ),"Tampines Street");
        $this->scrapeData($values,'HDB');
        //End Tampines

        //Yishun
        $values = $this->mergeHDBNameAndPostal(array(
            "1","11","2","3","4","5","6","7","9"
        ),"Yishun Avenue");
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "","1"
        ),"Yishun Central");
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "11","20","21","22","4","41","51","61","71","72","81"
        ),"Yishun Street");
        $this->scrapeData($values,'HDB');

        $values = array(
            "Yishun Ring Road"
        );
        $this->scrapeData($values,'HDB');
        //End Yishun

        //Yishun Ring Road
        $values = $this->mergeHDBNameAndPostal(array(
            "11","12","13","22","23","24"
        ),"Bishan Street");
        $this->scrapeData($values,'HDB');

        $values = array(
            "Bright Hill Drive",
            "Shunfu Road",
            "Sin Ming Avenue",
            "Sin Ming Road"
        );
        $this->scrapeData($values,'HDB');
        //End Yishun Ring Road

        //Bukit Panjang
        $values = array(
            "Bangkit Road",
            "Bukit Panjang Ring Road",
            "Fajar Road",
            "Gangsa Road",
            "Jelapang Road",
            "Jelebu Road",
            "Lompang Road",
            "Pending Road",
            "Petir Road",
            "Saujana Road",
            "Segar Road",
            "Senja Link",
            "Senja Road"
        );
        $this->scrapeData($values,'HDB');
        //End Bukit Panjang

        //Choa Chu Kang
        $values = $this->mergeHDBNameAndPostal(array(
            "1","2","3","4","5","7"
        ),"Choa Chu Kang Avenue");
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "5","6","7"
        ),"Choa Chu Kang North");
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "51","52","53","54","62","64"
        ),"Choa Chu Kang Street");
        $this->scrapeData($values,'HDB');

        $values = array(
            "Choa Chu Kang Central",
            "Choa Chu Kang Crescent",
            "Choa Chu Kang Drive",
            "Choa Chu Kang Loop",
            "Jalan Teck Whye",
            "Keat Hong Close",
            "Keat Hong Link",
            "Teck Whye Avenue",
            "Teck Whye Crescent",
            "Teck Whye Lane"
        );
        $this->scrapeData($values,'HDB');
        //End Choa Chu Kang

        //Hougang
        $values = array(
            "Buangkok Crescent",
            "Buangkok Green",
            "Buangkok Link",
            "Hougang Central",
            "Lorong Ah Soo",
            "Sumang Walk",
            "Upper Serangoon Crescent",
            "Upper Serangoon Road",
            "Upper Serangoon View"
        );
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "","1","10","2","3","4","5","6","7","8","9"
        ),"Hougang Avenue");
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "11","21","22","31","32","51","52","61","91","92"
        ),"Hougang Street");
        $this->scrapeData($values,'HDB');
        //End Hougang

        //Kallang/Whampoa
        $values = array(
            "Ah Hood Road",
            "Balestier Road",
            "Bendemeer Road",
            "Boon Keng Road",
            "Dorset Road",
            "Farrer Park Road",
            "Geylang Bahru",
            "Gloucester Road",
            "Jalan Bahagia",
            "Jalan Dusun",
            "Jalan Ma'mor",
            "Jalan Rajah",
            "Jalan Tenteram",
            "Kallang Bahru",
            "Kent Road",
            "Lorong 3 Geylang",
            "Lorong Limau",
            "Mcnair Road",
            "Owen Road",
            "Saint George's Lane",
            "Saint George's Road",
            "Towner Road",
            "Upper Boon Keng Road",
            "Whampoa Drive",
            "Whampoa Road",
            "Whampoa South",
            "Whampoa West"
        );
        $this->scrapeData($values,'HDB');
        //End Kallang/Whampoa

        //Punggol
        $values = array(
            "Edgedale Plains",
            "Edgefield Plains",
            "Punggol Central",
            "Punggol Drive",
            "Punggol East",
            "Punggol Field",
            "Punggol Field Walk",
            "Punggol Place",
            "Punggol Road",
            "Punggol Walk",
            "Punggol Way",
            "Sumang Lane",
            "Sumang Link",
            "Sumang Walk"
        );
        $this->scrapeData($values,'HDB');
        //End Punggol

        //Sengkang
        $values = array(
            "Fernvale Link",
            "Anchorvale Crescent",
            "Anchorvale Drive",
            "Anchorvale Lane",
            "Anchorvale Link",
            "Anchorvale Road",
            "Anchorvale Street",
            "Buangkok Link",
            "Buangkok South Farmway",
            "Buangkok South Farmway 1",
            "Compassvale Bow",
            "Compassvale Crescent",
            "Compassvale Drive",
            "Compassvale Lane",
            "Compassvale Link",
            "Compassvale Road",
            "Compassvale Street",
            "Compassvale Walk",
            "Fernvale Lane",
            "Fernvale Link",
            "Fernvale Road",
            "Fernvale Street",
            "Fernvale West Avenue",
            "Hougang Central",
            "Jalan Kayu",
            "Rivervale Crescent",
            "Rivervale Drive",
            "Rivervale Street",
            "Rivervale Walk",
            "Saint Anne's Wood",
            "Seletar West Farmway 6",
            "Sengkang Central",
            "Sengkang East Avenue",
            "Sengkang East Road",
            "Sengkang East Way",
            "Sengkang West Avenue",
            "Sengkang West Way"
        );
        $this->scrapeData($values,'HDB');
        //End Sengkang

        //Toa Payoh
        $values = array(
            "Jalan Tentram",
            "Joo Seng Road",
            "Kim Keat Avenue",
            "Kim Keat Link",
            "Lorong 1 Toa Payoh",
            "Lorong 2 Toa Payoh",
            "Lorong 3 Toa Payoh",
            "Lorong 4 Toa Payoh",
            "Lorong 5 Toa Payoh",
            "Lorong 6 Toa Payoh",
            "Lorong 7 Toa Payoh",
            "Lorong 8 Toa Payoh",
            "Moulmein Road",
            "Thomson Road",
            "Toa Payoh Central",
            "Toa Payoh East",
            "Toa Payoh North",
            "Toa Payoh Rise"
        );
        $this->scrapeData($values,'HDB');

        $values = $this->mergeHDBNameAndPostal(array(
            "1","2","3"
        ),"Potong Pasir Avenue");
        $this->scrapeData($values,'HDB');
        //End Toa Payoh
    }

    public function scrapeCondoCommercialDirectory(){
        $districts = array(
            "Boat Quay",
            "Raffles Place",
            "Marina",
            "Chinatown",
            "Tanjong Pagar",
            "Alexandra",
            "Commonwealth",
            "Harbourfront",
            "Telok Blangah",
            "Buona Vista",
            "West Coast",
            "Clementi New Town",
            "City Hall",
            "Clarke Quay",
            "Beach Road",
            "Bugis",
            "Rochor",
            "Farrer Park",
            "Serangoon Rd",
            "Orchard",
            "River Valley",
            "Tanglin",
            "Bukit Timah",
            "Holland",
            "Newton",
            "Novena",
            "Balestier",
            "Toa Payoh",
            "Macpherson",
            "Potong Pasir",
            "Eunos",
            "Paya Lebar",
            "Geylang",
            "East Coast",
            "Marine Parade",
            "Bedok",
            "Upper East Coast",
            "Changi Airport",
            "Changi Village",
            "Pasir Ris",
            "Tampines",
            "Hougang",
            "Sengkang",
            "Punggol",
            "Ang Mo Kio",
            "Thomson",
            "Bishan",
            "Clementi Park",
            "Upper Bukit Timah",
            "Boon Lay",
            "Tuas",
            "Jurong",
            "Dairy Farm",
            "Choa Chu Kang",
            "Bukit Panjang",
            "Lim Chu Kang",
            "Tengah",
            "Admiralty",
            "Woodlands",
            "Mandai",
            "Upper Thomson",
            "Sembawang",
            "Yishun",
            "Seletar",
            "Yio Chu Kang"
        );

        $this->scrapeData($districts,'CONDO');
    }

    public function mergeHDBNameAndPostal($postals, $hdbname){
        $values = [];
        foreach ($postals as $postal){
            $values[] = $hdbname. ' '. $postal;
        }
        return $values;
    }

    public function scrapeData($values, $type = ''){
        foreach ($values as $value) {
            sleep(10);

            $ch = curl_init();
            $url = 'https://agentnet.propertyguru.com.sg/ex_xmlhttp_propertysearch?source=listing&type=&q=' . $value;
            $agents = array(
                'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
                'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1.9) Gecko/20100508 SeaMonkey/2.0.4',
                'Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)',
                'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; da-dk) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1'
            );

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_USERAGENT, $agents[array_rand($agents)]);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $datas = json_decode(curl_exec($ch));
            curl_close($ch);


            if ($datas === NULL) {
                $response_json = array(
                    'status' => 'No data found! Try Again Later',
                    'message' => 'Failed'
                );

                echo json_encode($response_json);
                return;
            }

            foreach ($datas as $data) {
                $full_address = str_replace("'", "",$data->streetnumber . ' ' . $data->streetname);
                $json_data = str_replace("'", "", json_encode($data));
                $property_name = str_replace("'", "", $data->name);
                $property_type = str_replace("'", "", $data->typeCodeDescription);
                if ($type == $data->typeCode){
                    $loc_json = str_replace("'", "", $this->getLocJson($data->id));
                    $if_exists = Yii::app()->db->createCommand("SELECT * FROM xp_guru_properties WHERE pid='$data->id'")->queryAll();
                    echo 'Scraping Property => '. $data->name . '<br>';
                    if ($if_exists) {
                        Yii::app()->db->createCommand("UPDATE xp_guru_properties set json='$json_data', property_name= '$property_name', property_type = '$property_type', full_address = '$full_address', loc_json = '$loc_json' WHERE pid='$data->id'")->execute();
                        echo 'Successfully Scraped Property =>'. $data->name . '<br><br>';
                    } else {
                        Yii::app()->db->createCommand("INSERT INTO xp_guru_properties SET pid='$data->id' , json = '$json_data', property_name= '$property_name', property_type = '$property_type', full_address = '$full_address', loc_json = '$loc_json'")->execute();
                        echo 'Successfully Scraped Property =>'. $data->name . '<br><br>';
                    }

                    sleep(10);
                }else if($data->typeCode != "HDB" && $data->typeCode != "CONDO"){
                    $loc_json = str_replace("'", "", $this->getLocJson($data->id));
                    $if_exists = Yii::app()->db->createCommand("SELECT * FROM xp_guru_properties WHERE pid='$data->id'")->queryAll();
                    echo 'Scraping Property => '. $data->name . '<br>';
                    if ($if_exists) {
                        Yii::app()->db->createCommand("UPDATE xp_guru_properties set json='$json_data', property_name= '$property_name', property_type = '$property_type', full_address = '$full_address', loc_json = '$loc_json' WHERE pid='$data->id'")->execute();
                        echo 'Successfully Scraped Property =>'. $data->name . '<br><br>';
                    } else {
                        Yii::app()->db->createCommand("INSERT INTO xp_guru_properties SET pid='$data->id' , json = '$json_data', property_name= '$property_name', property_type = '$property_type', full_address = '$full_address', loc_json = '$loc_json'")->execute();
                        echo 'Successfully Scraped Property =>'. $data->name . '<br><br>';
                    }

                    sleep(10);
                }
            }

        }
    }

    public function getLocJson($pid)
    {

        $ch = curl_init();
        $url = 'https://agentnet.propertyguru.com.sg/ex_xmlhttp_propertysearch?property_id=' . $pid;

        $agents = array(
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
            'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1.9) Gecko/20100508 SeaMonkey/2.0.4',
            'Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)',
            'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; da-dk) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1'
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, $agents[array_rand($agents)]);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);

        $response_json = curl_exec($ch);
        curl_close($ch);

        return $response_json;

    }

}