<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
class CustomBuyController extends Controller
{

    public function redirect(Request $request){
        $error = 'Password is incorrect!';
        if($request->password == 'D888heers!'){
            return redirect('/domainDashboard');
        }else{
            return view('site.pages.password')->withErrors($error);
        }
    }

    public function domainDashboard(){
        return view ('site.layouts.domain-master');
    }

    public function queryDomains(){
        $domains = \DB::table('scraping.xp_domain_purchase_schedules')
                    ->get();

        return response()->json($domains);
    }

    public function addDomain(Request $request){
        $date = date("Y:m:d H:i:s");
        $checksum = md5("webcc-propnex123" . $date . md5("3Zj6F3sza9M3rnPu"));
        $param = [
            'reg_contact_type' => 1,
            'proxy' => 0,
            'custom_reg1' => '199903004H',
            'source' => 'webcc-propnex123',
            'username' => 'propnex123',
            'password' => 'ALUkyxdKvJU5$XC',
            'domainname' => $request->domain,
            'otime' => $date,
            'ochecksum' => $checksum,
            'term' => 1,
            'ns1' => 'ns1.web.cc',
            'ns2' => 'ns2.web.cc',
            'reg_company' => 'Propnex Realty Pte Ltd',
            'reg_fname' => 'Michael',
            'reg_lname' => 'Koh',
            'reg_addr1' => '480 Lorong 6 Toa Payoh',
            'reg_state' => 'Singapore',
            'reg_city' => 'Singapore',
            'reg_postcode' => '310480',
            'reg_telephone' => '+603.89966788',// %2B886.2579999
            'reg_country' => 'SG',
            'reg_email' => 'michael.koh@propnex.com',
            'adm_contact_type' => 1,//$_GET['adm_contact_type'],
            'adm_company' => 'Propnex Realty Pte Ltd',
            'adm_fname' => 'Michael',
            'adm_lname' => 'Koh',
            'adm_addr1' => '480 Lorong 6 Toa Payoh',
            'adm_state' => 'sg',
            'adm_city' => 'Singapore',
            'adm_postcode' => '310480',
            'adm_country' => 'sg',
            'adm_email' => 'michael.koh@propnex.com',
            'custom_adm1' => "199903004H",
            'adm_telephone' => '+603.89966788',//%2B886.2579999
            'tec_company' => 'Propnex Realty Pte Ltd',
            'tec_fname' => 'Michael',
            'tec_lname' => 'Koh',
            'tec_email' => 'michael.koh@propnex.com',
            'tec_telephone' => '+603.89966788',//%2B886.2579999
            'tec_addr1' => '480 Lorong 6 Toa Payoh',
            'tec_postcode' => '310480',
            'tec_country' => 'sg',
            'tec_city' => 'Singapore',
            'tec_contact_type' => 1,
            'tec_state' => 'sg',
            'bil_company' => 'Propnex Realty Pte Ltd',
            'bil_fname' => 'Michael',
            'bil_lname' => 'Koh',
            'bil_city' => 'Singapore',
            'bil_state' => 'Singapore',
            'bil_email' => 'michael.koh@propnex.com',
            'bil_country' => 'sg',
            'bil_addr1' => '480 Lorong 6 Toa Payoh',
            'bil_telephone' => '+603.89966788',//%2B886.2579999
            'bil_postcode' => '310480',
            'bil_contact_type' => 1,//$_GET['bil_contact_type'],
            'newuser' => "old",
            'custom_tec1' => "199903004H",
            'custom_bil1' => "199903004H",
        ];
        $params = http_build_query($param);
        $url =  'https://ote.webnic.cc/jsp/pn_newreg.jsp?';
        // $url = 'https://my.webnic.cc/jsp/pn_newreg.jsp?'; //production
      
        if(empty($request->schedule && $request->tries)){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close ($ch);
            $res = ltrim($result,$result[0]);
            $code = $result[0];
    
            $domain = $request->domain;
            if($code == 0){
                \DB::table('scraping.xp_domain_purchase_schedules')
                    ->insert([
                        'domain' => $request->domain,
                        'schedule' => null,
                        'isRegistered' => 1
                    ]);

                return response()->json($res);
            }
            else{
                $error = $result;
                return response()->json($error);
            }
        }else{
            $note = 'Domain '.$request->domain.' is added to database and scheduled to register on '.Carbon::parse($request->schedule)->toDayDateTimeString().'';
            \DB::table('scraping.xp_domain_purchase_schedules')
            ->insert([
                'domain' => $request->domain,
                'schedule' => $request->schedule,
                'tries' => $request->tries,
                'isRegistered' => 0
            ]);
            return response()->json($note);
        }
    }

    public function editDomain(Request $request){
        \DB::table('scraping.xp_domain_purchase_schedules')
            ->where('id',$request->id)
            ->update([
                'domain' => $request->domain,
                'tries' => $request->tries,
                'schedule' => $request->schedule,
                'isRegistered' => 0
            ]);
    }

    public function removeDomain (Request $request,$id){
       
        \DB::table('scraping.xp_domain_purchase_schedules')
            ->where('id', $id)
            ->delete();
    }

}
  