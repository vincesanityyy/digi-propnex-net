<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PayPalController extends Controller
{
    //
    public function index(Request $request){
        return view('site.pages.paypal')->with([
            "fname" => $request->fname,
            "lname" => $request->lname,
            "agentid" => $request->agentid,
            "auth" => $request->auth,
            "email" => $request->email,
            "prtname" => $request->prtname,
            "cea"=> $request->cea,
            "type" => $request->type,
            "staging"=> $request->staging,
            "price"=> $request->price,
            "domain" => $request->domain,
            'siteid' => $request->siteid,
            "ip"=> $request->ip,
            "isRenew" =>$request->isRenew
        ]);
    }

    public function createTransaction(Request $request){
        $host = $request->staging == 1 ? env('PROPNEX_IP') : env('PROPNEX_IP_PROD');
        $url = $host. '/propnex/index.php/domain/CreateTransaction?agentid='. $request->agentid .'&transaction_no='. $request->transaction_no .'&status='. $request->status . '&pay_for='. $request->pay_for;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);


        return $response;
    }

    public function customDomainRegistration(Request $request){
        $host = $request->staging == 1 ? env('PROPNEX_IP') : env('PROPNEX_IP_PROD');
        $query = http_build_query(array(
            'domainname' => $request->domainname,
            'reg_email' => $request->reg_email,
            'reg_fname' => $request->reg_fname,
            'reg_lname' => $request->reg_lname,
            'transaction_no' => $request->transaction_no,
            'agentid' => $request->agentid,
            'ns1' => $request->ns1,
            'ns2' => $request->ns2,
            'term' => 1,
//            'reg_contact_type' => $request->reg_contact_type,
//            'reg_company' => $request->reg_company,
            'reg_addr1' => $request->reg_addr1,
//            'reg_state' => $request->reg_state,
//            'reg_city' => $request->reg_city,
//            'reg_postal' => $request->reg_postal,
//            'reg_telephone' => $request->reg_telephone,
//            'reg_country' =>$request->reg_country
        ));


        $url = $host. '/propnex/index.php/domain/CustomDomain_registration?'. $query;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);


        return $response;
    }
    public function addDomain(Request $request){
        $host = $request->staging == 1 ? env('PROPNEX_IP') : env('PROPNEX_IP_PROD');
        $query = http_build_query(array(
            'domain' => $request->domain,
            'type' => $request->type,
            'expire-date' => $request->expiry_date,
            'site-id' => $request->site_id
        ));
        $url = $host. '/propnex/index.php/pnapi/addDomain?'. $query;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch,CURLOPT_HTTPHEADER,[
            'Authentication: '.$request->agentid . ':'. $request->auth
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);


        return $response;
    }

    public function ARecord(Request $request){
        $host = $request->staging == 1 ? env('PROPNEX_IP') : env('PROPNEX_IP_PROD');
        $query = http_build_query(array(
            'domain' => $request->domain,
            'ip1' => $request->ip1,
        ));
        $url = $host. '/propnex/index.php/domain/Arecord?'. $query;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);


        return $response;
    }

    public function domainRegistered(Request $request){
        $host = $request->staging == 1 ? env('PROPNEX_IP') : env('PROPNEX_IP_PROD');
        $query = http_build_query(array(
            'domain' => $request->domain,
            'siteid' => $request->siteid
        ));
        $url = $host. '/propnex/index.php/pnapi/domainRegistered?'. $query;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch,CURLOPT_HTTPHEADER,[
            'Authentication: '.$request->agentid . ':'. $request->auth
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);


        return $response;
    }

    public function Renewal_domain(Request $request){
        $host = $request->staging == 1 ? env('PROPNEX_IP') : env('PROPNEX_IP_PROD');
        $query = http_build_query(array(
            'domainname' => $request->domainname,
            'term' => $request->term,
            'transact_no' => $request->transact_no,
            'agentid' =>$request->agentid,
            'status' =>$request->status,
        ));
        $url = $host. '/propnex/index.php/domain/Renewal_domain?'. $query;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);


        return $response;
    }

    public function renewDomain (Request $request){
        $host = $request->staging == 1 ? env('PROPNEX_IP') : env('PROPNEX_IP_PROD');
        $query = http_build_query(array(
            'domain' => $request->domain,
            'expire-date' => $request->expiry_date,
            'site-id' => $request->siteid
        ));
        $url = $host. '/propnex/index.php/pnapi/renewDomain?'. $query;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch,CURLOPT_HTTPHEADER,[
            'Authentication: '.$request->agentid . ':'. $request->auth
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);


        return $response;
    }
}
