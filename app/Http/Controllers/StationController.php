<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StationController extends Controller
{
    /**
     * Circle line = Yellow
     * Downtowen Line = blue
     * East west line = green
     * north south line = red
     * north east line = purple
     * thomson-east coast line = black
     */
    public function searchMRT(Request $request){
        $result = \DB::table('scraping.xp_pn_mrt')
            ->where('mrt_name', 'LIKE', '%'.$request->station_name.'%')
            // ->select(\DB::raw('CONCAT(english_station_name," - ",operation_alpha_numeric_codes)as result'))
            ->select(\DB::raw('CONCAT(english_station_name," - ",REPLACE(operation_alpha_numeric_codes, "  ", " / "))as station_name'),
            \DB::raw('REPLACE(operation_alpha_numeric_codes,"  ", " / ")as code'),
            'latitude','longitude','x','y')
            ->where('operation_alpha_numeric_codes','!=','N/A')
            ->get();

            $colors = ['NS'=>'red','CC'=>'yellow','NE'=>'violet','DT'=>'blue','EW'=>'green','TE'=>'brown'];
            $result = json_decode($result,true);
            
            foreach($result as $key => $val){
                $color = [];
                foreach($colors as $k=>$v){
                    if(strpos($val['code'], $k) !== false){
                        $color[] = $v;
                    }
                }
                unset($val['code']);
                $result[$key] = $val + array('color'=>$color);
            }
            
            return $result;
    }

    public function searchLRT(Request $request){
        $result = \DB::table('scraping.xp_pn_lrt')
            ->where('lrt_name','LIKE','%'.$request->station_name.'%')
            ->select(\DB::raw('CONCAT(english_station_name," - ",REPLACE(alpha_numberical_code, "  "," / "))as station_name'),
            'latitude','longitude','x','y')
            ->get();
        foreach($result as $key => $value){
            $value->color='gray';
        }

        return response()->json($result);
    }

    // public function searchStationForMRT(Request $request){
    //     $result = \DB::table('scraping.xp_pn_mrt')
    //                 ->where('english_station_name', $request->station_name)
    //                 ->select(\DB::raw('CONCAT(english_station_name," - ",REPLACE(operation_alpha_numeric_codes, "  ", " / "))as station_name'),
    //                 \DB::raw('REPLACE(operation_alpha_numeric_codes,"  ", " / ")as code'),
    //                 'latitude','longitude','x','y')
    //                 ->where('operation_alpha_numeric_codes','!=','N/A')
    //                 ->get();
        
    //     $colors = ['NS'=>'red','CC'=>'yellow','NE'=>'violet','DT'=>'blue','EW'=>'green','TE'=>'brown'];
    //     $result = json_decode($result,true);
                    
    //         foreach($result as $key => $val){
    //                     $color = [];
    //                     foreach($colors as $k=>$v){
    //                         if(strpos($val['code'], $k) !== false){
    //                             $color[] = $v;
    //                 }
    //             }
    //             unset($val['code']);
    //             $result[$key] = $val + array('color'=>$color);
    //         }
    //         return $result;
    // }

    public function searchStationByStationName(Request $request){
        $result = \DB::table('scraping.xp_pn_lrt')
                    ->where('english_station_name','LIKE','%'.$request->station_name.'%')
                    ->select(\DB::raw('CONCAT(english_station_name," - ",REPLACE(alpha_numberical_code, "  ", " / "))as station_name'),
                    \DB::raw('REPLACE(alpha_numberical_code,"  ", " / ")as code'),
                    'lrt_name as line_name','latitude','longitude','x','y')
                    ->get();

        foreach($result as $key => $value){
            $color = [];
            $value->isLRT ='true';
            $value->color[] = 'gray';
        }

        $result_2 = \DB::table('scraping.xp_pn_mrt')
                    ->where('english_station_name','LIKE','%'.$request->station_name.'%')
                    ->select(\DB::raw('CONCAT(english_station_name," - ",REPLACE(operation_alpha_numeric_codes, "  ", " / "))as station_name'),
                    \DB::raw('REPLACE(operation_alpha_numeric_codes,"  ", " / ")as code'),
                    'mrt_name as line_name','latitude','longitude','x','y','english_station_name')
                    ->where('operation_alpha_numeric_codes','!=','N/A')
                    ->groupBy('english_station_name')
                    ->get();

                    $colors = ['NS'=>'red','CC'=>'yellow','NE'=>'violet','DT'=>'blue','EW'=>'green','TE'=>'brown'];
                    $result_2 = json_decode($result_2,true);
                                
                        foreach($result_2 as $key => $val){
                                    
                                    $color = [];
                                    foreach($colors as $k=>$v){
                                        if(strpos($val['code'], $k) !== false){
                                            $color[] = $v;
                                }
                            }
                            unset($val['code']);
                            unset($val['english_station_name']);
                            $result_2[$key] = $val + array('color'=>$color,'isLRT' =>'false');
                        }
                        // return $result_2;
      
        $final_res = $result->merge($result_2);            
        return $final_res;
    }


}
