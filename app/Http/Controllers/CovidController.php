<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CovidController extends Controller
{
    public function showForm(Request $request){
        $agent_id = $request->agent_id;
       
        return view('site.pages.covidForm',compact('agent_id'));
    }


    public function submitForm(Request $request){

        $agent_id = 3;

        $agent_api = 'https://s3.ap-southeast-1.amazonaws.com/propnex-xserver-img/dotcom/agent/'.$agent_id.'.json';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $agent_api );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
        $x = json_decode($response,true);
        $email = $x['prtemail'];
        
        if($email == 'NA' || $email == ''){
            $email = 'ronald.b@filipinowebmasters.com';
        }
        // dd($email);
       
       
        
        $css = 'health/css/bootstrap.min.css';
        $data_type = pathinfo($css, PATHINFO_EXTENSION);
        $css_data = file_get_contents($css);

        $path = 'health/img/logo.png';
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
     

        $fullname = $request->agent_name;                       
        $phone = $request->phone;
        $nationality = $request->nationality;
        $have_not_travelled_to_china  = $request->have_not_travelled_to_china;
        $have_not_travelled_to_others = $request->have_not_travelled_to_others;
        $have_not_travelled_to_asian = $request->have_not_travelled_to_asian;
        $no_issue_to_stay_home  = $request->no_issue_to_stay_home;
        $no_symptomps = $request->no_symptomps;
        $dont_have_close_contact = $request->dont_have_close_contact;
        $signDate = $request->signDate;
        $date =$request->date;
    //    dd($request->date);
        $arr = array(
            'css_data' => $css_data,
            'base64' => $base64
        );
        $pdf = \PDF::loadView('emails.form',compact('fullname','phone','nationality','have_not_travelled_to_china','have_not_travelled_to_others',
            'have_not_travelled_to_asian','no_issue_to_stay_home','no_symptomps','dont_have_close_contact',
            'signDate','date','base64','css_data'
        ))->setPaper('a4')->setOrientation('portrait')->setOption('margin-bottom', 0);
     
       
        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
        $beautymail->send('emails.welcome', [], function($message)use($pdf)
        {
            $message
                ->from('testemail.ideahub@gmail.com','TEST')
                ->to('ronald.b@filipinowebmasters.com')
                ->attachData($pdf->output(), "HealthCare.pdf")
                ->subject('HEALTH DECLARATION FORM');
        });
        // $inputs = \DB::table('scraping.xp_health_table')->insert([
        //     'agent_id' => '',
        //     'agent_name' => $request->agent_name,
        //     'phone' => $request->phone,
        //     'nationality' => $request->nationality,
        //     'have_not_travelled_to_china' => $request->have_not_travelled_to_china,
        //     'have_not_travelled_to_others' => $request->have_not_travelled_to_others,
        //     'have_not_travelled_to_asian' => $request->have_not_travelled_to_asian,
        //     'no_issue_to_stay_home' => $request->no_issue_to_stay_home,
        //     'no_symptomps' => $request->no_symptomps,
        //     'dont_have_close_contact' => $request->dont_have_close_contact,
        //     'signDate' => $request->signDate,
        //     'date' => $request->date
        // ]);
        // return response()->json($inputs);
    }
    
    public function showSubmissions(){
        $results = \DB::table('scraping.xp_health_table')
                    ->orderBy('date','desc')
                    ->get();
        foreach($results as $data){
            if($data->have_not_travelled_to_china === 1){
                $data->have_not_travelled_to_china = '<i style="color:green" class="fa fa-check fa-lg" aria-hidden="true"></i>';
            }else{
                $data->have_not_travelled_to_china = '<i style="color:red" class="fa fa-circle-o fa-lg" aria-hidden="true"></i>';
            }
            if($data->have_not_travelled_to_others === 1){
                $data->have_not_travelled_to_others = '<i style="color:green" class="fa fa-check fa-lg" aria-hidden="true"></i>';
            }else{
                $data->have_not_travelled_to_others = '<i style="color:red" class="fa fa-circle-o fa-lg" aria-hidden="true"></i>';
            }
            if($data->have_not_travelled_to_asian === 1){
                $data->have_not_travelled_to_asian = '<i style="color:green" class="fa fa-check fa-lg" aria-hidden="true"></i>';
            }else{
                $data->have_not_travelled_to_asian = '<i style="color:red" class="fa fa-circle-o fa-lg" aria-hidden="true"></i>';
            }
            if($data->no_issue_to_stay_home === 1){
                $data->no_issue_to_stay_home = '<i style="color:green" class="fa fa-check fa-lg" aria-hidden="true"></i>';
            }else{
                $data->no_issue_to_stay_home = '<i style="color:red" class="fa fa-circle-o fa-lg" aria-hidden="true"></i>';
            }
            if($data->no_symptomps === 1){
                $data->no_symptomps = '<i style="color:green" class="fa fa-check fa-lg" aria-hidden="true"></i>';
            }else{
                $data->no_symptomps = '<i style="color:red" class="fa fa-circle-o fa-lg" aria-hidden="true"></i>';
            }
            if($data->dont_have_close_contact === 1){
                $data->dont_have_close_contact = '<i style="color:green" class="fa fa-check fa-lg" aria-hidden="true"></i>';
            }else{
                $data->dont_have_close_contact = '<i style="color:red" class="fa fa-circle-o fa-lg" aria-hidden="true"></i>';
            }
        }
       
        return response()->json(array(
            'data' =>$results
        ));
    }


    public function submissionList(){
        $result = \DB::table('scraping.xp_health_table')
            ->select('id','agent_name','signDate')
            ->get();

        foreach($result as $data){
            $data->signDate = Carbon::createFromFormat('d/m/Y',$data->signDate)->format('M j, Y');
        }
        return $result;
    }

    public function getSubmissionDetails(Request $request){
        $result = \DB::table('scraping.xp_health_table')
            ->select('agent_name','nationality','phone','date','have_not_travelled_to_china','have_not_travelled_to_others',
            'have_not_travelled_to_asian','no_issue_to_stay_home','no_symptomps','dont_have_close_contact')
            ->where('id',$request->id)
            ->get();
        foreach($result as $data){
            $data->date = Carbon::createFromFormat('d/m/Y g:i A',$data->date)->format('M j, Y g:i A');
        }

        return $result;
    }
}
