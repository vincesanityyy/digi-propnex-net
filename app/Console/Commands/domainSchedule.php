<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
class domainSchedule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'domain:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register Domain in Webnic';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        print('Date: '.date('Y-m-d H:i')."\n");
     
        $domain = \DB::table('scraping.xp_domain_purchase_schedules')
            ->select('domain','tries')
            ->where('isRegistered',0)
            // ->whereDate('schedule', Carbon::now()->format('Y-m-d H:i'))
            ->whereRaw(\DB::raw("DATE_FORMAT(schedule,'%Y-%m-%d %H:%i') = DATE_FORMAT(now(),'%Y-%m-%d %H:%i')"))
            // ->whereDate('schedule', Carbon::now()->toDateString())
            ->get();
        print($domain."\n");
        foreach ($domain as $data){
            
            $date = date("Y:m:d H:i:s");
            $checksum = md5("webcc-propnex123" . $date . md5("3Zj6F3sza9M3rnPu"));
            $url =  'https://ote.webnic.cc/jsp/pn_newreg.jsp?';
            $param = [
                'reg_contact_type' => 1,
                'proxy' => 0,
                'custom_reg1' => '199903004H',
                'source' => 'webcc-propnex123',
                'username' => 'propnex123',
                'password' => 'ALUkyxdKvJU5$XC',
                'domainname' => $data->domain,
                'otime' => $date,
                'ochecksum' => $checksum,
                'term' => 1,
                'ns1' => 'ns1.web.cc',
                'ns2' => 'ns2.web.cc',
                'reg_company' => 'Propnex Realty Pte Ltd',
                'reg_fname' => 'Michael',
                'reg_lname' => 'Koh',
                'reg_addr1' => '480 Lorong 6 Toa Payoh',
                'reg_state' => 'Singapore',
                'reg_city' => 'Singapore',
                'reg_postcode' => '310480',
                'reg_telephone' => '+603.89966788',// %2B886.2579999
                'reg_country' => 'SG',
                'reg_email' => 'michael.koh@propnex.com',
                'adm_contact_type' => 1,//$_GET['adm_contact_type'],
                'adm_company' => 'Propnex Realty Pte Ltd',
                'adm_fname' => 'Michael',
                'adm_lname' => 'Koh',
                'adm_addr1' => '480 Lorong 6 Toa Payoh',
                'adm_state' => 'sg',
                'adm_city' => 'Singapore',
                'adm_postcode' => '310480',
                'adm_country' => 'sg',
                'adm_email' => 'michael.koh@propnex.com',
                'custom_adm1' => "199903004H",
                'adm_telephone' => '+603.89966788',//%2B886.2579999
                'tec_company' => 'Propnex Realty Pte Ltd',
                'tec_fname' => 'Michael',
                'tec_lname' => 'Koh',
                'tec_email' => 'michael.koh@propnex.com',
                'tec_telephone' => '+603.89966788',//%2B886.2579999
                'tec_addr1' => '480 Lorong 6 Toa Payoh',
                'tec_postcode' => '310480',
                'tec_country' => 'sg',
                'tec_city' => 'Singapore',
                'tec_contact_type' => 1,
                'tec_state' => 'sg',
                'bil_company' => 'Propnex Realty Pte Ltd',
                'bil_fname' => 'Michael',
                'bil_lname' => 'Koh',
                'bil_city' => 'Singapore',
                'bil_state' => 'Singapore',
                'bil_email' => 'michael.koh@propnex.com',
                'bil_country' => 'sg',
                'bil_addr1' => '480 Lorong 6 Toa Payoh',
                'bil_telephone' => '+603.89966788',//%2B886.2579999
                'bil_postcode' => '310480',
                'bil_contact_type' => 1,//$_GET['bil_contact_type'],
                'newuser' => "old",
                'custom_tec1' => "199903004H",
                'custom_bil1' => "199903004H",
            ];
            
            $counter = $data->tries;
            $tries = 0;
            do {
                sleep(5);
                $params = http_build_query($param);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch);
                $res = ltrim($result,$result[0]);
                $code = $result[0];
                if($code == 0){
                    print('Domain '.$data->domain.' is registered!'. "\n");
                    $success = \DB::table('xp_domain_purchase_schedules')
                                ->where('domain',$data->domain)
                                ->update(['isRegistered' => 1]);
                    break;
                }else{
                    print('Trying to save domain '.$data->domain. "\n");
                    $tries++;
                }
            } while ($tries <= $counter);
                if($tries >= $counter){
                    print('Failed to save domain  '.$data->domain. ' after '.$data->tries.' tries'."\n");
                    $failed = \DB::table('xp_domain_purchase_schedules')
                        ->where('domain',$data->domain)
                        ->update(['isRegistered' => 3]);
                }else{
                    continue; 
                }
            
        
        }
       
    }
}
