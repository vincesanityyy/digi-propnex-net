<?php

namespace App\Console\Commands;

use App\Transactions;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class everyHour extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hour:adddomain';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will automatically add the domain to propnex server if ping is successfull';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
//        $transactions = Transactions::all();
        $transactions = Transactions::withoutTrashed()->get();
        Log::info($transactions);
        foreach ($transactions as $transaction){
            $ping = $this->pingDomain($transaction);

            if ($ping == 0){
                $host = $transaction->staging == 1 ? env('PROPNEX_IP') : env('PROPNEX_IP_PROD');
                $url = $host .'/propnex/index.php/pnapi/addBuyDomain?agentid='. $transaction->agentid .'&domain='. $transaction->domain .'&site-id='. $transaction->site_id .'&expire-date='. $transaction->expire_date;

                echo $url;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url );
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $response = json_decode(curl_exec($ch));

                if($response->status != 'fail'){
//                    $message = 'added domain successfully : '. $transaction->domain;
//
//                    $log = response()->json([
//                        "status" => "ok",
//                        "message" => $message,
//                        "date" => $date
//                    ]);
//                    echo $log;
//                    Log::info($log);
                    $transaction->delete();
                }else{
                    $log = response()->json([
                        "status" => "fail",
                        "message" => $response->error,
//                        "date" => $date
                    ]);
                    echo $log;
                    Log::info($log);
                }
            }else{
                $message = 'unable to ping domain :'. $transaction->domain;
                $log = response()->json([
                    "status" => "fail",
                    "message" => $message,
//                    "date" => $date
                ]);
                echo $log;
                Log::info($log);
            }
        }
    }

    private function pingDomain($transaction){
        $host = $transaction->staging == 1 ? env('PROPNEX_IP') : env('PROPNEX_IP_PROD');
        $url = $host. '/propnex/index.php/domain/Ping_domain?domain='.urlencode($transaction->domain).'&host_ip='.$transaction->ip;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch,CURLOPT_HTTPHEADER,[
            'Content-type: application/json'
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = json_decode(curl_exec($ch));

        return $response->status;
    }
}
