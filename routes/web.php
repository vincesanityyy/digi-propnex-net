<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('site.pages.covidForm');
});
Route::get('/form', function(){
    return view('emails.form');
});

Route::any('buy-domain', 'BuyDomainController@index')->name('buy-domain');
Route::get('confirm-domain', 'ConfirmDomainController@index')->name('confirm-domain');
Route::get('get-domain', 'ConfirmDomainController@getDomain')->name('get-domain');
Route::get('renew-domain' , 'BuyDomainController@renewDomain');
Route::get('payment','PayPalController@index')->name('payment');

//Route::get('add-own-domain', 'ConfirmDomainController@addOwnDomain')->name('add-own-domain');
Route::get('add-own-domain', 'DomainOwnController@showInstructions')->name('add-own-domain');
Route::get('success-domain-own', 'ConfirmDomainController@successDomainOwn')->name('success-domain-own');
Route::get('ping-domain', 'DomainOwnController@pingDomain')->name('ping-domain');
Route::get('ping-domain-fail', 'DomainOwnController@pingDomainfail')->name('ping-domain-fail');
Route::get('ping-domain-success', 'DomainOwnController@pingDomainSuccess')->name('ping-domain-success');

Route::get('success-domain', 'DomainOwnController@index')->name('success-domain');
Route::get('instructions', 'DomainOwnController@showInstructions')->name('instructions');

Route::get('verify-email','ConfirmDomainController@verifyEmail')->name('verify-email');

// Route::get('/test','DomainController@actionGetDomain');



Route::get('/propnexcredentials', function(){
    return view('site.pages.password');
});

Route::get('/customDomainRegistration', 'CustomBuyController@showForm')->name('custom');
Route::get('/registerDomain', 'CustomBuyController@registerDomain');
Route::get('/deleteDomain','CustomBuyController@deleteDomain');
Route::get('/redirect','CustomBuyController@redirect');

Route::post('/registerDomainWithSchedule', 'CustomBuyController@registerWithSchedule');

Route::get('/domainlist','CustomBuyController@getDomains');
Route::delete('/deleteDomain','CustomBuyController@deleteDomain');
Route::post('/updateDomain','CustomBuyController@updateDomain');

Route::get('/domainDashboard','CustomBuyController@domainDashboard');
Route::get('/queryDomains','CustomBuyController@queryDomains');
Route::post('/addDomain','CustomBuyController@addDomain');
Route::put('editDomain/{id?}','CustomBuyController@editDomain');
Route::delete('deleteDomain/{id?}','CustomBuyController@removeDomain');

//health
Route::get('/healthForm/{agent_id?}', 'CovidController@showForm');
Route::post('/submitForm','CovidController@submitForm');
Route::get('/showSubmissions','CovidController@showSubmissions');
Route::get('/submissions', function(){
    return view('site.pages.covid-dashboard');
});