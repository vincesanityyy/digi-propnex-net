<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('createTransaction', 'PayPalController@createTransaction');
Route::get('customDomainRegistration', 'PayPalController@customDomainRegistration');
Route::get('addDomain','PayPalController@addDomain');
Route::get('Arecord','PayPalController@ARecord');
Route::get('domainRegistered','PayPalController@domainRegistered');
Route::get('Renewal_domain','PayPalController@Renewal_domain');
Route::get('renewDomain','PayPalController@renewDomain');

Route::post('addRegisteredDomain', 'TransactionsController@store');
Route::get('getRegisterDomains/{agentid}', 'TransactionsController@getRegisterDomains');
Route::get('removeDomain/{id}','TransactionsController@destroy');

//for testing
Route::get('addBuyDomain','TransactionsController@addBuyDomain');

//MRT LRT
Route::get('searchMRT','StationController@searchMRT');
Route::get('searchLRT','StationController@searchLRT');
// Route::get('searchStationMRT','StationController@searchStationForMRT');
Route::get('searchByStationName','StationController@searchStationByStationName');

//School
Route::get('/searchBySchool','SchoolController@searchSchool');
Route::get('/schoolRank','SchoolController@orderRank');

// Covid
Route::get('/submissionList','CovidController@submissionList');
// getSubmissionDetails
Route::get('/getSubmissionDetails{id?}','CovidController@getSubmissionDetails');

//Transaction Histor
Route::get('/transactionHistory','TransactionHistorycontroller@getTransactionHistory');

  