<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">

      <link rel="stylesheet" href="{{asset('datetimepicker/css/tail.datetime-default-blue.css')}}">
      <script src="{{asset('datetimepicker/js/tail.datetime.js')}}"></script>

      <title>Document</title>
   </head>
   <body>
      <div class="container">
          <br><br><br><br><br><br>
         <div class="card text-center">
            <div class="card-header">
               Domain List
            </div>
            <div class="card-body">
               <table id="myTable" class="table table-hover table-bordered">
                  <thead>
                     <tr>
                        <th scope="col">Domain</th>
                        <th scope="col">Schedule</th>
                        <th scope="col">Status</th>
                        <th scope="col">Tries</th>
                        <th scope="col">Actions</th>
                     </tr>
                  </thead>
                  <tbody>
                    @foreach($domain as $data)
                     <tr>
                        <td>{{$data->domain}}</td>
                        <td>{{\Carbon\Carbon::parse($data->schedule)->toDayDateTimeString()}}</td>
                        @if($data->isRegistered == 0)
                        <td>Waiting for schedule</td>
                        @elseif($data->isRegistered == 1)
                        <td>Registered</td>
                        @else
                        <td>Aborted</td>
                        @endif
                        <td>{{$data->tries}}</td>
                        <td>
                            <button  edit_domain_id = {{$data->id}} domain_name={{$data->domain}} domain_tries = {{$data->tries}} domain_schedule = {{$data->schedule}} class="btn btn-warning editDomain">Update</button>
                            <button  domain_id="{{$data->id}}" class="btn btn-danger deleteDomain">Delete</button>
                        </td>
                     </tr>
                  </tbody>
                  @endforeach
               </table>
            </div>
            <div class="card-footer ">
                <a href="{{ route('custom') }}"class="btn btn-block btn-primary">Add Domain</a>
            </div>
         </div>
      </div>
      {{-- Modal --}}
    <div class="modal fade bd-example-modal-sm" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form method="POST" action=" {!! url('/deleteDomain') !!}" >
            {{ method_field('delete') }}
            {{ csrf_field() }}
            <div class="modal-body">
              <p>Delete Domain?</p>
              <input type="hidden" name="domain_id" value=""  id="domain_id">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button submit="button" class="btn btn-danger">Delete</button>
            </div>
          </div>
        </form>
        </div>
    </div>
    {{-- modal --}}
    {{-- Edit modal --}}
    <div class="modal fade bd-example-modal-sm" id="exampleModal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Update</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form method="POST" action=" {!! url('/updateDomain') !!}" >
          {{ method_field('post') }}
          {{ csrf_field() }}
          <div class="modal-body">
            <div class="form-group">
               <input type="hidden" name="edit_domain_id" value=""  id="edit_domain_id">
               <label for="">Domain Name</label>
               <input class="form-control" type="text" name="domain_name" id="domain_name" value="" >
               <label for="">Schedule</label>
               <input type="text" placeholder="Schedule" name="date" id="date" class="demo form-control">
               <label for="">Tries</label>
               <input class="form-control" name="domain_tries" type="text" id="domain_tries" value="" >
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button submit="button" class="btn btn-primary">Update</button>
          </div>
        </div>
      </form>
      </div>
  </div>
    {{-- edit modal --}}
   </body>
   <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" 
   integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" 
   integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" 
   integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
 
   <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
   <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" 
   integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
   </html>
   
   <script>
       $(document).ready( function () {
            $('#myTable').DataTable({
                responsive: {
                    details: false
                }
            });
            $(document).on('click','.deleteDomain',function(){
                var domainId=$(this).attr('domain_id');
                document.getElementById('domain_id').value = domainId;
               //  $('#doman_id input').val(domainId); 
                $('#exampleModal').modal('show'); 
                console.log(domainId)
            });

            $(document).on('click','.editDomain',function(){
                var domainId=$(this).attr('edit_domain_id');
               //  var domainSchedule=$(this).attr('domain_schedule');
                var domainTries=$(this).attr('domain_tries');
                var domainName=$(this).attr('domain_name');

                document.getElementById('edit_domain_id').value = domainId;
                document.getElementById('domain_tries').value = domainTries;
                document.getElementById('domain_name').value = domainName;
               //  document.getElementById('domain_schedule').value = domainSchedule;
               
                $('#exampleModal_edit').modal('show'); 
               //  console.log(domainId)
            });

            tail.DateTime("#date", {
            dateFormat: "YYYY-mm-dd",
            timeFormat: "HH:ii:ss",
            animate: true,  
            closeButton: true,
            stayOpen: true
            });
        });
   </script>