@extends('site.layouts.app')

@section('content')
    <div class="d-flex justify-content-center h-100 paypal-page">
        <div class="user_card">

            <div class="d-flex justify-content-center" style="text-align: center">
                <div class="header">
                    @if($isRenew == 1)
                        <b><p class="paypal-description">Domain renewal of  {{$domain}} for {{$fname}} Cea Number:{{$cea}}</p></b>
                    @else
                        <b><p class="paypal-description">Domain registration of  {{$domain}} for {{$fname}} Cea Number:{{$cea}}</p></b>
                    @endif
                </div>
            </div>

            <div class="d-flex justify-content-center mb-3">
                <div class="header">
                    <h1>$<span class="paypal-amount">{{$price}}</span></h1>
                </div>
            </div>

            <div class="d-flex justify-content-center">
                <input type="hidden" id="staging" value="{{$staging}}">
                <input type="hidden" id="agentid" value="{{$agentid}}">
                <input type="hidden" id="auth" value="{{$auth}}">
                <input type="hidden" id="fname" value="{{$fname}}">
                <input type="hidden" id="lname" value="{{$lname}}">
                <input type="hidden" id="email" value="{{$email}}">
                <input type="hidden" id="domain" value="{{$domain}}">
                <input type="hidden" id="prtname" value="{{$prtname}}">
                <input type="hidden" id="siteid" value="{{$siteid}}">
                <input type="hidden" id="ip" value="{{$ip}}">
                <div class="brand_logo_container">
                    <a href="#">
                        <img src="./assets/img/logo.png" class="brand_logo" alt="Logo">
                    </a>
                </div>
                <div id="paypal-button-container"></div>
            </div>

            <!-- The Modal -->
            <div id="successModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Transaction</h4>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            Payment Transaction Successfully.
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="./js/paypal.js" ></script>
@endsection