@extends('site.layouts.app')

@section('content')
    <!-- success page --->
    <div class="d-flex justify-content-center h-100 success-page">
        <div class="user_card">

            <div class="d-flex justify-content-center">
                <div class="brand_logo_container">
                    <a href="#">
                        <img src="./assets/img/logo.png" class="brand_logo" alt="Logo">
                    </a>
                </div>
            </div>

            <div class="d-flex justify-content-center">
                <div class="brand_icon_container mt-5">
                    <img src="./assets/img/icn_correct.png" class="brand_icon" alt="Logo">
                    <h1>Domain</h1>
                    <h2>{{$domain}}</h2>
                </div>
            </div>

            <div style="margin-top: 330px;">
                <div class="mt-5" style="background-color:#EEEEEE; text-align:center; padding-top: 5px;">
                    <h3 >Connecting Domain</h3>
                    <div class="d-flex justify-content-center">
                        <p>
                            Connecting your third-party domain to Propnex DotCom, points your domain name at your own site.
                            You still use the third-party domain provider to manage your domain settings, pay for your domain,
                            and renew it.
                        </p>
                    </div>
                    <form id= "instruction-form" action="{{route('instructions')}}" method="GET">
                        <input type="hidden" name="fname" value="{{$fname}}">
                        <input type="hidden" name="lname" value="{{$lname}}">
                        <input type="hidden" name="email" value="{{$email}}">
                        <input type="hidden" name="prtname" value="{{$prtname}}">
                        <input type="hidden" name="agentid" value="{{$agentid}}">
                        <input type="hidden" name="auth" value="{{$auth}}">
                        <input type="hidden" name="cea" value="{{$cea}}">
                        <input type="hidden" name="staging" value="{{$staging}}">
                        <input type="hidden" name="domain" value="{{$domain}}">
                        <input type="hidden" name="ip" value="{{$ip}}">
                        <input type="hidden" name="siteid" value="{{$siteid}}">
                        <input type="hidden" name="isRenew" value="{{$isRenew}}">
                        <button type="submit" class="btn ok-btn btn-proceed" style="width: auto;" data-toggle="modal">View Instruction</button>
                    </form>

                    <div class="d-flex justify-content-center">
                        <p>
                            Verify connection to make sure your domain is set up correctly
                        </p>
                    </div>
                    <form action="{{route('ping-domain')}}" method="GET">
                        <input type="hidden" name="fname" value="{{$fname}}">
                        <input type="hidden" name="lname" value="{{$lname}}">
                        <input type="hidden" name="email" value="{{$email}}">
                        <input type="hidden" name="prtname" value="{{$prtname}}">
                        <input type="hidden" name="agentid" value="{{$agentid}}">
                        <input type="hidden" name="auth" value="{{$auth}}">
                        <input type="hidden" name="cea" value="{{$cea}}">
                        <input type="hidden" name="staging" value="{{$staging}}">
                        <input type="hidden" name="domain" value="{{$domain}}">
                        <input type="hidden" name="ip" value="{{$ip}}">
                        <input type="hidden" name="siteid" value="{{$siteid}}">
                        <input type="hidden" name="isRenew" value="{{$isRenew}}">
                    <button type="submit" class="btn ok-btn btn-proceed" style="width: auto;">Verify Connection</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection