<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <link rel="stylesheet" href="{{asset('health/css/bootstrap.min.css')}}">
    <link href="{{asset('health/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('health/css/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('health/css/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <title>Health Form</title>
</head>
<body>
    <div id="app" class="container">
        
    <covid-form agent="{{$agent_id}}">
         
        </covid-form>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="{{asset('health/js/bootstrap.min.js')}}"></script>
<script src="{{asset('health//js/js.js')}}" type="text/javascript"></script>
<script src="js/app.js"></script>
</html>