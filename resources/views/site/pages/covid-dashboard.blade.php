<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <title>Covid Form Submissions</title>
</head>
<style>
    td.details-control {
    background: url('/health/img/details_open.png') no-repeat center center;
    cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('/health/img/details_close.png') no-repeat center center;
    }
</style>

<body>
    <div class="container">
        <br>

        <div class="card text-center">
            <div class="card-header">
                Covid-19 Form Submissions
            </div>
            <div class="card-body">
                <table id="myTable" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Agent ID</th>
                            <th>Agent Name</th>
                            <th>Nationality</th>
                            <th>Handphone No.</th>
                            <th>Date of Submission</th>
                        </tr>
                    </thead>
                    <tbody>
                </table>
            </div>
        </div>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

</html>

<script>
    $(document).ready(function() {

        function check() {

        }

        function format(d, tes) {
            // `d` is the original data object for the row
            return '<table  class="table table-hover table-bordered" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                '<tr>' +
                '<th colspan="3">SECTION 1: Travel Declaration (Prior to 20th March 2020, 23:59)</th>' +
                '</tr>' +
                '<tr>' +
                '<td>' + d.have_not_travelled_to_china + '</td>' +
                '<td>I declare that I HAVE NOT travelled to Mainland China in the past 14 days.</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' + d.have_not_travelled_to_others + '</td>' +
                '<td>I declare that I HAVE NOT travelled to Republic of Korea, France, Germany, Italy, Iran, Spain, Japan, Switzerland and The United Kingdom in the past 14 days.</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' + d.have_not_travelled_to_asian + '</td>' +
                '<td>I declare that I HAVE NOT travelled to ASEAN Countries in the past 14 days.</td>' +
                '</tr>' +
                '<tr>' +
                '<th colspan="3">SECTION 2: Travel Declaration (Effective from 20th March 2020, 23:59)</th>' +
                '</tr>' +
                '<tr>' +
                '<td>' + d.no_issue_to_stay_home + '</td>' +
                '<td>I declare that I AM NOT issued with a 14-day Stay-Home Notice (SHN).</td>' +
                '</tr>' +
                '<tr>' +
                '<th colspan="3">SECTION 3: General Health Declaration</th>' +
                '</tr>' +
                '<tr>' +
                '<td>' + d.no_symptomps + '</td>' +
                '<td>I declare that I AM WELL and have no symptoms of fever, running nose and cough.</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' + d.dont_have_close_contact + '</td>' +
                '<td>I declare that I DO NOT HAVE ANY CLOSE CONTACT with persons (e.g. immediate family, housemates, relatives, friends) who have returned from Mainland China or the countries above OR had any contact with those who had COVID 19.</td>' +
                '</tr>' +
                '</table>';
        }

        var table = $('#myTable').DataTable({
            "ajax": "/showSubmissions",
            "columns": [{
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '',
                }, {
                    "data": "agent_id"
                }, {
                    "data": "agent_name"
                }, {
                    "data": "nationality"
                }, {
                    "data": "phone"
                }, {
                    "data": "date"
                },

            ],
        });

        $('#myTable tbody').on('click', 'td.details-control', function() {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });
    });
</script>