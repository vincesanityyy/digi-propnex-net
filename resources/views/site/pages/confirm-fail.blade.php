@extends('site.layouts.app')

@section('content')
    <!-- Confirm domain page --->
    <div class="d-flex justify-content-center h-100 confirm-domain">
        <div class="user_card">
            <div class="d-flex justify-content-center">
                <div class="brand_logo_container">
                    <a href="#">
                        <img src="./assets/img/logo.png" class="brand_logo" alt="Logo">
                    </a>
                </div>
                <div class="brand_icon_container mt-5">
                    <img src="./assets/img/icn_oops.png" class="brand_icon" alt="Logo">
                </div>
            </div>

            <div class="d-flex justify-content-center form_container search-container">
                <form action="{{route('confirm-domain')}}" method="GET">
                    <input type="hidden" name="fname" value="{{$fname}}">
                    <input type="hidden" name="lname" value="{{$lname}}">
                    <input type="hidden" name="email" value="{{$email}}">
                    <input type="hidden" name="prtname" value="{{$prtname}}">
                    <input type="hidden" name="agentid" value="{{$agentid}}">
                    <input type="hidden" name="auth" value="{{$auth}}">
                    <input type="hidden" name="type" value="{{$type}}">
                    <input type="hidden" name="domain" value="{{$domain}}">
                    <input type="hidden" name="cea" value="{{$cea}}">
                    <input type="hidden" name="staging" value="{{$staging}}">
                    <input type="hidden" name="ip" value="{{$ip}}">
                    <input type="hidden" name="siteid" value="{{$siteid}}">
                    <input type="hidden" name="isRenew" value="{{$isRenew}}">
                    <div class="input-group mb-3 justify-content-center md-form form-sm form-2 pl-0 mt-5">
                        <h2><b>OOPS!</b></h2>
                    </div>
                    <div class="input-group justify-content-center md-form form-sm form-2 pl-0">
                        <h2><b>{{$domain}}</b></h2>
                    </div>
                    <div class="input-group justify-content-center md-form form-sm form-2 pl-0">
                        <h3>is not available, Please try again</h3>
                    </div>

                    <div class="d-flex justify-content-center mt-4 btn_container">
                        <button type="submit" class="btn ok-btn">
                            SEARCH AGAIN
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>

@endsection