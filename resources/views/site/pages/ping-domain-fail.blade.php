@extends('site.layouts.app')

@section('content')
    <!-- success page --->
    <div class="d-flex justify-content-center h-100 success-page">
        <div class="user_card">
            <div class="d-flex justify-content-center">
                <div class="brand_logo_container">
                    <a href="#">
                        <img src="./assets/img/logo.png" class="brand_logo" alt="Logo">
                    </a>
                </div>
            </div>

            <div class="d-flex justify-content-center">
                <div class="brand_icon_container mt-5">
                    <img src="./assets/img/icn_oops.png" class="brand_icon" alt="Logo">
                    <div class="mt-5">
                        <b>
                            <p>
                                Your existing domain did not resolve to our IP yet. Please ensure that you have configured your DNS settings at your domain registrar.
                            </p>
                        </b>

                        <form action="{{route('instructions')}}" method="GET">
                            <input type="hidden" name="fname" value="{{$fname}}">
                            <input type="hidden" name="lname" value="{{$lname}}">
                            <input type="hidden" name="email" value="{{$email}}">
                            <input type="hidden" name="prtname" value="{{$prtname}}">
                            <input type="hidden" name="agentid" value="{{$agentid}}">
                            <input type="hidden" name="auth" value="{{$auth}}">
                            <input type="hidden" name="cea" value="{{$cea}}">
                            <input type="hidden" name="staging" value="{{$staging}}">
                            <input type="hidden" name="domain" value="{{$domain}}">
                            <input type="hidden" name="ip" value="{{$ip}}">
                            <input type="hidden" name="siteid" value="{{$siteid}}">
                            <input type="hidden" name="isRenew" value="{{$isRenew}}">
                            <button type="submit" class="btn ok-btn btn-proceed">RETRY</button>
                        </form>
                    </div>

                </div>
            </div>

            <div class="d-flex justify-content-center mt-4 btn_container">

            </div>

        </div>
    </div>
@endsection