@extends('site.layouts.app')

@section('content')
    <!-- Buy domain page --->
    <div class="d-flex justify-content-center h-100 buy-domain">
        <div class="user_card">
            <div class="d-flex justify-content-center">
                <div class="brand_logo_container">
                    <a href="#">
                        <img src="./assets/img/logo.png" class="brand_logo" alt="Logo">
                    </a>

                </div>
            </div>

            @if($status == true)
                <div class="d-flex justify-content-center">
                    <div class="brand_icon_container mt-5">
                        <img src="./assets/img/icn_correct.png" class="brand_icon" alt="Logo">
                    </div>
                </div>

                <div class="verify-container">
                    <h1 style="color: green">Success!</h1>
                    <p>Your domain request has been sent to your email <b>{{$email}}</b> </p>
                </div>
            @endif

            @if($status == false)
                <div class="d-flex justify-content-center">
                    <div class="brand_icon_container mt-5">
                        <img src="./assets/img/icn_oops.png" class="brand_icon" alt="Logo">
                    </div>
                </div>

                <div class="verify-container">
                    <h1 style="color: red; margin-top: 25px">Fail!</h1>
                    <p>Error upon sending domain request please try again!</p>

                    <div class="d-flex justify-content-center btn_container">
                        <form action="{{route('verify-email')}}" method="GET">
                            <input type="hidden" name="fname" value="{{$fname}}">
                            <input type="hidden" name="lname" value="{{$lname}}">
                            <input type="hidden" name="email" value="{{$email}}">
                            <input type="hidden" name="prtname" value="{{$prtname}}">
                            <input type="hidden" name="agentid" value="{{$agentid}}">
                            <input type="hidden" name="auth" value="{{$auth}}">
                            <input type="hidden" name="cea" value="{{$cea}}">
                            <input type="hidden" name="staging" value="{{$staging}}">
                            <input type="hidden" name="ip" value="{{$ip}}">
                            <input type="hidden" name="siteid" value="{{$siteid}}">
                            <input type="hidden" name="type" value="buy">
                            <button type="submit" class="btn ok-btn" style="width: 150px;">
                                Try Again
                            </button>
                        </form>
                    </div>
                </div>
            @endif

        </div>
    </div>
@endsection

