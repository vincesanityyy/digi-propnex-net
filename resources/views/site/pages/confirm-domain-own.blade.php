@extends('site.layouts.app')

@section('content')
    <!-- Confirm domain page --->
    <div class="d-flex justify-content-center h-100 confirm-domain">
        <div class="user_card">
            <div class="d-flex justify-content-center">
                <div class="brand_logo_container">
                    <a href="#">
                        <img src="./assets/img/logo.png" class="brand_logo" alt="Logo">
                    </a>
                </div>
                <div class="brand_icon_container mt-5">
                    <img src="./assets/img/icn_search.png" class="brand_icon" alt="Logo">
                </div>
            </div>

            <div class="d-flex justify-content-center form_container search-container">
                <form action="{{route('add-own-domain')}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="fname" value="{{$fname}}">
                    <input type="hidden" name="lname" value="{{$lname}}">
                    <input type="hidden" name="email" value="{{$email}}">
                    <input type="hidden" name="prtname" value="{{$prtname}}">
                    <input type="hidden" name="agentid" value="{{$agentid}}">
                    <input type="hidden" name="auth" value="{{$auth}}">
                    <input type="hidden" name="cea" value="{{$cea}}">
                    <input type="hidden" name="staging" value="{{$staging}}">
                    <input type="hidden" name="ip" value="{{$ip}}">
                    <input type="hidden" name="siteid" value="{{$siteid}}">
                    <input type="hidden" name="isRenew" value="{{$isRenew}}">
                    <input type="hidden" name="type" value="{{$type}}">

                    <div class="input-group mb-3 md-form form-sm form-2 pl-0 ">
                        <h2>Let's Confirm your domain first</h2>
                    </div>
                    @if($status == false)
                        <div class="d-flex justify-content-center btn_container">
                            <p style="color:red">{{$message}}</p>
                        </div>
                    @endif
                    <div class="input-group md-form form-sm form-2 pl-0 search-form">
                        <input type="text" class=" search-query form-control input_user" name="domain" placeholder="propnex.com.sg"
                               aria-label="Search"/>
                        <span class="input-group-btn">
                          <button class="btn" type="button">
                              <span class="glyphicon glyphicon-search"></span>
                          </button>
                      </span>
                    </div>

                    <div class="d-flex justify-content-center mt-4 btn_container">
                        <button type="submit" class="btn ok-btn">
                            OK
                        </button>
                    </div>
                </form>

            </div>

        </div>
    </div>
@endsection