<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="{{asset('datetimepicker/css/tail.datetime-default-blue.css')}}">
    <script src="{{asset('datetimepicker/js/tail.datetime.js')}}"></script>

    <style>
        .datepicker td,
        .datepicker th {
            width: 2.5rem;
            height: 1.5rem;
            font-size: 0.85rem;
        }
        
        .datepicker {
            margin-bottom: 1rem;
        }
        
        .datepicker-dropdown {
            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
        }
    </style>
    <title>Document</title>
</head>

<body>
    <div class="container">
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <div class="card text-center">
            <div class="card-header">
                <h6>Enter Domain Name</h6>
            </div>
            <div  class="card-body">
                <form action=" {!! url('/registerDomain') !!}"  id="submitForm">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input required type="text" class="form-control" name="domain" id="domain" aria-describedby="emailHelp" placeholder="Enter domain">
                    </div>
                    <div class="form-group">
                        <input type="text" placeholder="Schedule" name="date" id="date" class="demo form-control">
                    </div>
                    <div class="form-group">
                        <input  type="number" min="1" class="form-control" name="tries" id="tries" aria-describedby="emailHelp" placeholder="Tries">
                        @if(count($errors) > 0)
                        <div id="error" class="alert alert-danger">
                            @foreach($errors->all() as $error)
                            <strong>{{ $error }}</strong> @endforeach
                        </div>
                        @endif
                        @if(session()->has('message'))
                            <div id="success" class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                    </div>
                    
                    <button id="submit" type="submit" class="btn btn-primary">Submit</button>
                    <a href="/customDomainReqistration" class="btn btn-info">To Dashboard</a>
                </form>
            </div>
        </div>
    </div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

</html>

<script>
    $(document).ready(function() {
        setTimeout(function() {
            $('#error').fadeOut('slow');
        }, 2000);
        setTimeout(function() {
            $('#success').fadeOut('slow');
        }, 2000);

        tail.DateTime("#date", {
        dateFormat: "YYYY-mm-dd",
        timeFormat: "HH:ii:ss",
        animate: true,  
        closeButton: true,
        stayOpen: true
        });
    });

    $('.datepicker').datepicker({
        clearBtn: true,
        format: "yyyy-mm-dd"
    });

    // function test(event){
    //     event.preventDefault();
    //     var date = document.getElementById('date');
    //     var domain = document.getElementById('domain');
    //     console.log(date.value)
    //     console.log(domain.value)
    // }

     
</script>