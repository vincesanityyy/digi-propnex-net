@extends('site.layouts.app')

@section('content')
    <!-- success page --->
    <div class="d-flex justify-content-center h-100 success-page">
        <div class="user_card">
            <div class="d-flex justify-content-center">
                <div class="brand_logo_container">
                    <a href="#">
                        <img src="./assets/img/logo.png" class="brand_logo" alt="Logo">
                    </a>
                </div>
            </div>

            <div class="d-flex justify-content-center">
                <div class="brand_icon_container mt-5">
                    <img src="./assets/img/icn_correct.png" class="brand_icon" alt="Logo">
                </div>
            </div>
            <div class="d-flex justify-content-center form_container note-container">

                <div class="content">
                    <h1>Bingo!</h1>
                    <p><span class="registered-domain">{{$domain}}</span> is available for</p>
                </div>
                <div class="add_container mt-5 justify-content-center">
                    <div class="d-flex justify-content-center container_price" style="position:absolute;padding-top: 100px;width: 100%">
                        <table>
                            <tr>
                                <td> <img style="height: 50px; width: 30px;" src="./assets/img/price_beforeselector.png" class="brand_logo" alt="Logo"></td>
                                <td><h2 style="color: #ffffff; font-size: 35px;"> ${{$price}}</h2></td>
                                <td><img style="height: 50px; width: 30px;" src="./assets/img/price_afterselector.png" class="brand_logo" alt="Logo"></td>
                            </tr>
                        </table>


                    </div>
                    <img src="./assets/img/no-price_03.png" class="add_icon" alt="Logo">
                </div>

            </div>

            <div class="d-flex justify-content-center mt-4 btn_container">

                <form action="{{route('payment')}}" method="GET">
                    <input type="hidden" name="fname" value="{{$fname}}">
                    <input type="hidden" name="lname" value="{{$lname}}">
                    <input type="hidden" name="email" value="{{$email}}">
                    <input type="hidden" name="prtname" value="{{$prtname}}">
                    <input type="hidden" name="agentid" value="{{$agentid}}">
                    <input type="hidden" name="auth" value="{{$auth}}">
                    <input type="hidden" name="cea" value="{{$cea}}">
                    <input type="hidden" name="staging" value="{{$staging}}">
                    <input type="hidden" name="price" value="{{$price}}">
                    <input type="hidden" name="domain" value="{{$domain}}">
                    <input type="hidden" name="ip" value="{{$ip}}">
                    <input type="hidden" name="siteid" value="{{$siteid}}">
                    <input type="hidden" name="isRenew" value="{{$isRenew}}">
                    <button type="submit" class="btn ok-btn btn-proceed">PROCEED TO REGISTER</button>
                </form>
            </div>

        </div>
    </div>
@endsection