<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
    <div class="container">
          <br><br><br><br><br><br>
          <div class="card text-center border-success">
            {{-- <div class="card-header">
                <h6>Enter Domain Name</h6>
            </div> --}}
            <div class="card-body text-success">
                <form action=" {!! url('/registerDomain') !!}" id="submitForm"> 
              <div class="form-group">
                <p class="card-text">Domain name {{$domain}} is available! Enter the validity of the domain (in years)</p>
                <input required type="hidden" class="form-control" 
                name="domain" id="domain" value={{$domain}}>
                <input required type="text" class="form-control" 
                name="year" id="year" aria-describedby="emailHelp" placeholder="">
                @if(count($errors) > 0)
                <div id="error" class="alert alert-danger">
                  @foreach($errors->all() as $error)
                    <strong>{{ $error }}</strong>
                        @endforeach
                </div>
               @endif
              </div>
              <a href="{{ URL::previous() }}" class="btn btn-danger">Back</a>
              <button id="submit" type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
          </div>
    </div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
{{-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
</html>


<script>
    $(document).ready(function(){
    setTimeout(function() {
    $('#error').fadeOut('slow');
        }, 2000); 
    });
</script>