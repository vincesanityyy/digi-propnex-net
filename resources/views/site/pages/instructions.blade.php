@extends('site.layouts.app')

@section('content')
    <div class="d-flex justify-content-center h-100 confirm-domain">
        <div class="own_card">
            {{--<div class="d-flex justify-content-center">--}}
                {{--<div class="brand_logo_container">--}}
                    {{--<a href="#">--}}
                        {{--<img src="./assets/img/logo.png" class="brand_logo" alt="Logo">--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}


            <div class="d-flex justify-content-center">
                <div class="brand_icon_container" style="margin-top: 60px;">
                    <div class="d-flex justify-content-center">
                        <h2>2 Methods - How To Connect Your Own Domain</h2>
                    </div>
                    <div style="text-align: left;">
                        <label>1st method</label>
                        <hr/>
                        <ol style="margin-left: -28px;">
                            <li>Log in to the account you have with your domain provider.<br/><br/></li>
                            <li>Find your DNS settings. Look for your domain management area, DNS configuration, or
                                similar.<br/><br/></li>
                            <li>Edit your A record to point to PropNex DotCom IP address {{$ip}} Copy The Domain settings panel
                                might feature drop-downs or editable fields, or you might have to check boxes or edit a table.
                                You need to add 2 A Records point to IP Address below:<br/><br/></li>
                            <ol>
                                {{--<li>enter or choose the <b>@</b> symbol, or <b>A record</b></li>--}}
                                {{--<li>enter PropNex DotCom’s IP address {{$ip}} Copy as the destination for the A record.<br/><br/></li>--}}
                                <li> @ to {{$ip}}</li>
                                <li> www to {{$ip}}<br/><br/></li>
                            </ol>
                            <li>Save the A record (click <b>Save, Save Zone File, Add Record,</b> or similar).</li>
                        </ol>

                        <label>2nd method</label>
                        <hr/>

                        <p style="font-size:medium">
                            If you are unsure how to do it,  you can click here to email your registrar to help you.
                            We have a template for you. Take note that not all registrar will help you.
                            <a href="mailto:test@example.com?subject=Assist to add A records to my {{isset($domain)? $domain : '[domain-name]'}}&body=Dear Registrar,%0d%0a%0d%0aRegarding my {{isset($domain) ? $domain: '[domain-name]'}},%0d%0a%0d%0aPlease add a new A record in our DNS below:%0d%0a%0d%0a 1. {{'@'}} to {{isset($ip)? $ip : '[ip address]'}}%0d%0a 2. www to {{isset($ip)? $ip : '[ip address]'}}%0d%0a%0d%0a Thanks and best regards,%0d%0a{{$fname}}{{$lname}}">[EMAIL REGISTRAR]</a>
                        </p>
                        <hr/>

                    </div>

                    <form action="{{route('ping-domain')}}" method="GET">
                        <input type="hidden" name="fname" value="{{$fname}}">
                        <input type="hidden" name="lname" value="{{$lname}}">
                        <input type="hidden" name="email" value="{{$email}}">
                        <input type="hidden" name="prtname" value="{{$prtname}}">
                        <input type="hidden" name="agentid" value="{{$agentid}}">
                        <input type="hidden" name="auth" value="{{$auth}}">
                        <input type="hidden" name="cea" value="{{$cea}}">
                        <input type="hidden" name="staging" value="{{$staging}}">
                        <input type="hidden" name="ip" value="{{$ip}}">
                        <input type="hidden" name="siteid" value="{{$siteid}}">
                        <input type="hidden" name="isRenew" value="{{$isRenew}}">
                        <input type="hidden" name="type" value="own">

                        @if($status == false)
                            <div class="d-flex justify-content-center btn_container">
                                <p style="color:red">{{$message}}</p>
                            </div>
                        @endif
                        <label for="verify" style="font-size: medium">
                            If it is done, please enter your domain here to start verify
                        </label>
                        <div class="input-group md-form form-sm form-2 pl-0 search-form">
                            <input id="verify" type="text" class="  search-query form-control input_user" name="domain" placeholder="eg. propnex.com.sg"
                                   value="{{$domain}}" aria-label="Search"/>
                            <span class="input-group-btn">
                              <button class="btn" type="button">
                                  <span class="glyphicon glyphicon-search"></span>
                              </button>
                            </span>
                        </div>

                        <div class="d-flex justify-content-center mt-4 btn_container">
                            <button type="submit" class="btn ok-btn mb-3">
                                VERIFY
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection