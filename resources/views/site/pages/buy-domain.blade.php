@extends('site.layouts.app') @section('content')
<!-- Buy domain page --->
<div class="d-flex justify-content-center h-100 buy-domain">
    <div class="user_card">
        <div class="d-flex justify-content-center">
            <div class="brand_logo_container">
                <a href="#">
                    <img src="./assets/img/logo.png" class="brand_logo" alt="Logo">
                </a>

            </div>
        </div>
        <div class="d-flex justify-content-center">
            @if($device == 'android')
            <div class="header">
                <h1>BUY NEW DOMAIN</h1>
            </div>
            @else
            <div class="header">
                <h1>CONNECT YOUR DOMAIN</h1>
            </div>
            @endif
            <div class="brand_icon_container mt-5">
                <img src="./assets/img/icn_buy_new_domain@3x.png" class="brand_icon" alt="Logo">
            </div>
        </div>

        <div class="d-flex justify-content-center form_container new-domain-container">
            <div class="content">
                <p>Make your property stand out and easy to find with custom domain</p>
            </div>
        </div>
        @if($status == false)
        <div class="d-flex justify-content-center btn_container">
            <p style="color:red">{{$message}}</p>
        </div>
        @endif

        @if($device == 'android')
        <form action="{{route('confirm-domain')}}" method="GET">
            <div class="d-flex justify-content-center mt-4 btn_container">
                <input type="hidden" name="fname" value="{{$fname}}">
                <input type="hidden" name="lname" value="{{$lname}}">
                <input type="hidden" name="email" value="{{$email}}">
                <input type="hidden" name="prtname" value="{{$prtname}}">
                <input type="hidden" name="agentid" value="{{$agentid}}">
                <input type="hidden" name="auth" value="{{$auth}}">
                <input type="hidden" name="cea" value="{{$cea}}">
                <input type="hidden" name="staging" value="{{$staging}}">
                <input type="hidden" name="ip" value="{{$ip}}">
                <input type="hidden" name="siteid" value="{{$siteid}}">
                <input type="hidden" name="type" value="buy">
        
                <button type="submit" class="btn ok-btn">
                    Buy New Domain
                </button>
            </div>
        </form>
        <div class="d-flex justify-content-center mt-4 ">
                <p style="font-size: 16px;">or</p>
            </div>
        @else
        <form action="{{route('verify-email')}}" method="GET">
            <div class="d-flex justify-content-center mt-4 btn_container">
                <input type="hidden" name="fname" value="{{$fname}}">
                <input type="hidden" name="lname" value="{{$lname}}">
                <input type="hidden" name="email" value="{{$email}}">
                <input type="hidden" name="prtname" value="{{$prtname}}">
                <input type="hidden" name="agentid" value="{{$agentid}}">
                <input type="hidden" name="auth" value="{{$auth}}">
                <input type="hidden" name="cea" value="{{$cea}}">
                <input type="hidden" name="staging" value="{{$staging}}">
                <input type="hidden" name="ip" value="{{$ip}}">
                <input type="hidden" name="siteid" value="{{$siteid}}">
                <input type="hidden" name="type" value="buy">
                <button type="submit" class="btn ok-btn">
                    Request New Domain
                </button>
              
            </div>
        </form>
        <div class="d-flex justify-content-center mt-4 ">
            <p style="font-size: 16px;">or</p>
        </div>
        @endif
        <div class="d-flex justify-content-center ">
            <a href="{{route('add-own-domain')}}?fname={{$fname}}&lname={{$lname}}&email={{$email}}&prtname={{$prtname}}&agentid={{$agentid}}&auth={{$auth}}&type=own&staging={{$staging}}&cea={{$cea}}&siteid={{$siteid}}&ip={{$ip}}" class="existing-domain">Connect to your existing domain</a>
        </div>
    </div>
</div>
@endsection