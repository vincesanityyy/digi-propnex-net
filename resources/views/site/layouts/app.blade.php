<!DOCTYPE html>
<html lang="en">

@include('site.inc.header')
<body>
    <div class="container h-100">
        @yield('content')
    </div>
    @include('site.inc.footer')
</body>


</html>