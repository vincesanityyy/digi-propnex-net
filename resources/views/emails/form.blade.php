<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <style>
         {{$css_data}}
      </style>
      <style>
         .main-wrapper {
         max-width: 800px;
         margin: 20px auto;
         font-size: 18px;
         text-align: justify;
         border: 1px solid #f4f4f4;
         padding: 0px 35px;
         }
         .img-center {
         margin: 20px auto;
         display: block;
         max-width: 200px;
         }
         h3 {
         text-align: center;
         font-weight: bold;
         }
         .input-field {
         border: 0px;
         border: 1px solid #cacaca;
         width: 100%;
         border-radius: 4px;
         height: 40px;
         margin-top: -5px;
         }
         input:focus {
         outline: 0px;
         }
         h4 {
         font-weight: bold;
         font-size: 22px;
         }
         .align-items-center p {
         margin-bottom: 0px;
         margin-left: 15px;
         }
         .align-items-start p {
         margin-bottom: 0px;
         margin-left: 15px;
         width: calc(100% - 30px);
         }
         .align-items-start input {
         margin-top: 5px;
         width: 30px;
         }
         .signaturedate {
         width: 100%;
         float: left;
         margin-top: 50px;
         margin-bottom: 50px;
         }
         .signaturedate p {
         position: relative;
         top: 10px;
         margin-right: 10px;
         }
         .image-upload input {
         opacity: 0;
         width: 100%;
         float: left;
         height: 50px;
         }
         .image-upload {
         width: 100%;
         float: left;
         height: 50px;
         border-bottom: 1px solid #333;
         }
         .date-input {
         width: 100%;
         float: left;
         height: 50px;
         border: 0px;
         border-bottom: 1px solid #333;
         }
         .border-h {
         border-top: 1px solid #cacaca;
         padding-top: 30px;
         }
         .smt-btn {
         max-width: 200px;
         width: 100%;
         float: left;
         background: #27aeee;
         border-radius: 10px;
         padding: 15px;
         text-align: center;
         color: white;
         margin-top: 40px;
         border: 0px;
         }
         @media(max-width:767px) {
         .row {
         margin: 0px;
         }
         .main-wrapper {
         padding: 0px 15px;
         margin: 0px;
         background: white;
         }
         body {
         padding: 15px;
         background: #f4f4f4;
         }
         .form {
         margin-top: -20px;
         margin-left: -15px;
         margin-right: -15px;
         }
         .form p {
         margin-bottom: -5px;
         margin-top: 5px;
         }
         .signaturedate p {
         position: relative;
         top: 10px;
         margin-right: 10px;
         width: 120px;
         }
         }
         @media print {
         .main-wrapper {
         padding: 0px;
         margin: 0px auto;
         border: 0px;
         }
         .input-field {
         border: 0px;
         border-bottom: 1px solid #cacaca;
         width: 100%;
         border-radius: 4px;
         height: 40px;
         margin-top: -5px;
         }
         .smt-btn {
         display: none;
         }
         .border-h {
         border-top: 0px solid #cacaca;
         padding-top: 00px;
         }
         }
      </style>
   </head>
   <body>
     <div class="container">
        <div class="main-wrapper ">
            <img src="{{$base64}}" class="img img-fluid img-center">
            <h3>HEALTH DECLARATION FORM</h3>
            <p class="mt-4">In light of the COVID 19 situation and the surge in the number of confirmed cases around the world, we would like to assure that precautionary measures have been put in place to safeguard the well-being of our salespersons and clients. This information here is to also for contact tracing purposes if the need arises.</p>
            <br><br>
           
               <div class="col-md-3  mt-3">
                  <p>Full Name:</p>
               </div>
               <div class="col-md-9  mt-3">
                  <input value="{{$fullname}}" type="text" class="form-control">
               </div>
               <div class="col-md-3  mt-3">
                  <p>Nationality:</p>
               </div>
               <div class="col-md-9  mt-3">
                  <input value="{{$nationality}}" type="text" class="form-control">
               </div>
               <div class="col-md-3  mt-3">
                  <p>Handphone No:</p>
               </div>
               <div class="col-md-9 mt-3">
                  <input value="{{$phone}}" type="text" class="form-control">
               </div>
               <div class="col-md-5  mt-3">
                  <p>Date & Time of viewing/ meeting:</p>
               </div>
               <div class="col-md-7  mt-3">
               <input value="{{$date}}"type="text" class="form-control">
               </div>
          
            <h4 class="mt-4 border-h">SECTION 1: Travel Declaration (Prior to 20th March 2020, 23:59)</h4>
            <p>Please declare accordingly:</p>
            <div class="d-flex w-100 align-items-start mb-4">
                @if($have_not_travelled_to_china == true)
                <input checked type="checkbox">
                @endif
               <p> I declare that <b>I HAVE NOT</b> travelled to <b>Mainland China</b> in the past 14 days.</p>
            </div>
            <div class="d-flex w-100 align-items-start mb-4">
                @if($have_not_travelled_to_others == true)
                <input checked type="checkbox">
                @endif
               <p> I declare that <b>I HAVE NOT</b> travelled to <b>Republic of Korea, France, Germany, Italy, Iran,
                  Spain, Japan, Switzerland and The United Kingdom</b> in the past 14 days.
               </p>
            </div>
            <div class="d-flex w-100 align-items-start mb-4">
               @if($have_not_travelled_to_asian == true)
               <input checked type="checkbox">
               @endif
               <p> I declare that <b>I HAVE NOT</b> travelled to <b>ASEAN Countries^</b> in the past 14 days.</p>
            </div>
            <h4 class="mt-4 border-h">SECTION 2: Travel Declaration (Effective from 20th March 2020, 23:59)</h4>
            <div class="d-flex w-100 align-items-start mb-4">
                @if($no_issue_to_stay_home == true)
                <input checked type="checkbox">
                @endif
               <p> I declare that <b>I AM NOT</b> issued with a 14-day Stay-Home Notice (SHN).</p>
            </div>
            <br><br><br>
            <h4 class="mt-4 border-h">SECTION 3: General Health Declaration</h4>
            <div class="d-flex w-100 align-items-start mb-4">
                @if($no_symptomps == true)
                <input checked type="checkbox">
                @endif
               <p> I declare that <b>I AM WELL</b> and have no symptoms of fever, running nose and cough.</p>
            </div>
            <div class="d-flex w-100 align-items-start mb-4">
                @if($dont_have_close_contact == true)
                <input checked type="checkbox">
                @endif
               <p> I declare that <b>I DO NOT HAVE ANY CLOSE CONTACT</b><br> with persons (e.g. immediate family, housemates, relatives, friends) who have returned from Mainland China or the countries above <b>OR</b> had any contact with those who had COVID 19.</p>
            </div>
          
               <div class="row">
               <p>Date</p>
                <input value="{{$signDate}}" type="text" class="form-control">
               </div>
           
            <p class="mt-5">Does not apply to Singaporeans and Malaysians travelling into Singapore from Malaysia via sea or land </p>
            <p>Updated 19 March 2020</p>
            <p class="text-right">RCP-FM-19-01 19/03/20</p>
         </div>
     </div>
   </body>
</html>