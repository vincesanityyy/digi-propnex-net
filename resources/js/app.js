require('./bootstrap');
Vue.prototype.$toast = toast
window.Vue = require('vue');
import Vue from 'vue'
import VueRouter from 'vue-router'
import { Form, HasError, AlertError } from 'vform'
import datePicker from 'vue-bootstrap-datetimepicker';
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';
import swal from 'sweetalert2'
import Loading from 'vue-loading-overlay';
// Import stylesheet
import 'vue-loading-overlay/dist/vue-loading.css';

Vue.use(Loading);
Vue.use(VueRouter)
Vue.use(require('vue-moment'));
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
Vue.use(datePicker);

window.Form = Form;
window.swal = swal;
window.toast = toast;

Vue.component('domain-dashboard', require('./components/Domain-Schedule/DomainDashboard.vue').default);
Vue.component('covid-form', require('./components/Covid19-Form/CovidForm.vue').default);

const routes = [
    { path: '/domains', component: require('./components/Domain-Schedule/DomainDashboard.vue').default },
]

const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    width: 200,
    timer: 3000
});


  

const router = new VueRouter({
    mode: 'history',
    routes
})


const app = new Vue({
    el: '#app',
    router,
    components: {
        datePicker,
    }
});
