/**
 * Created by patal-patrik on 29/07/2019.
 */
(function ($) {

    $(document).ready(function(){
        console.log('ready');
        paypal.Buttons({
            createOrder: function(data, actions) {
                var description = $('.paypal-description')[0].innerText,
                    value = $('.paypal-amount')[0].innerText;


                // Set up the transaction
                return actions.order.create({
                    purchase_units: [{
                        description: description,
                        amount: {
                            value: value
                        }
                    }]
                });
            },

            onApprove: function(data, actions) {
                // Capture the funds from the transaction

                actions.order.capture().then(function(details) {
                    // Show a success message to your buyer

                    var domain_info = {
                            agentid : $('#agentid').val(),
                            domain : $('#domain').val(),
                            email : $('#email').val(),
                            fname : $('#fname').val(),
                            lname:  $('#lname').val(),
                            prtname: $('#prtname').val(),
                            auth: $('#auth').val(),
                            ip: $('#ip').val(),
                            staging: $('#staging').val(),
                            siteid: $('#siteid').val(),
                        },
                        status = details.status === 'COMPLETED' ? 'paid' : '',
                        transaction_no = details.id,
                        expiry_date = '',
                        exists = 0;

                    $.ajax({
                        type: "GET",
                        url: window.location.protocol + '//'+ window.location.host +'/api/createTransaction?agentid=' + domain_info.agentid + '&transaction_no=' + transaction_no+ '&status=' + status + '&pay_for=' + domain_info.domain + '&staging=' + domain_info.staging,
                        async: false
                    });

                    var customDomainRegistration =  $.ajax({
                        type: "GET",
                        url: window.location.protocol + '//' + window.location.host +'/api/customDomainRegistration',
                        data: {
                            domainname:domain_info.domain,
                            reg_email: domain_info.email,
                            reg_fname: domain_info.fname === '' || domain_info.fname === null || domain_info.fname === undefined ? domain_info.prtname : domain_info.fname,
                            reg_lname: domain_info.lname,
                            transaction_no: transaction_no,
                            agentid: domain_info.agentid,
                            // pay_for: domain_info.domain,
                            // term: 1,
                            ns1: domain_info.staging === 1? 'ns1.web.cc' : 'ns1.ezydomain.com' ,
                            ns2: domain_info.staging === 1? 'ns2.web.cc' : 'ns2.ezydomain.com',
                            // reg_contact_type: 1,
                            // reg_company: 'PropNex Realty Pte Ltd',
                            reg_addr1: '480 Lorong 6 Toa Payoh, #10-01',
                            // reg_state: 'Singapore',
                            // reg_city: 'Singapore',
                            // reg_postal: '310480',
                            // reg_telephone: '+682.08000',
                            // reg_country: 'SG',
                            staging: domain_info.staging
                        },
                        async:false,
                        success:function(data){
                            expiry_date = data.message;
                        }
                    });

                    expiry_date = JSON.parse(customDomainRegistration.responseText).message.replace("0\t","");
                    expiry_date = expiry_date.replace("\n","");
                    exists = JSON.parse(customDomainRegistration.responseText).message.indexOf('already registered');

                    if(exists !== -1){

                        var renewalDomain = $.ajax({
                            type: "GET",
                            url: window.location.protocol + '//' + window.location.host +'/api/Renewal_domain',
                            async: false,
                            data: {
                                domainname: domain_info.domain,
                                term: 1,
                                transact_no: details.id,
                                agentid: domain_info.agentid,
                                status: 'renew',
                                staging: domain_info.staging
                            },
                            success:function(data){
                                expiry_date = data.message;
                            }
                        });


                        expiry_date = JSON.parse(renewalDomain.responseText).message;
                        $.ajax({
                            type: "GET",
                            url: window.location.protocol + '//' + window.location.host +'/api/renewDomain',
                            async: false,
                            data: {
                                domain: domain_info.domain,
                                expiry_date: expiry_date,
                                auth: domain_info.auth,
                                siteid: domain_info.siteid,
                                agentid: domain_info.agentid,
                                staging: domain_info.staging
                            },
                            success: function(data){
                                console.log(data);
                            }
                        });

                        $('#successModal').modal('show');
                    }else{

                        var body = {
                            domain: domain_info.domain,
                            ip: domain_info.ip,
                            agentid: domain_info.agentid,
                            site_id: domain_info.siteid,
                            staging: domain_info.staging,
                            expire_date: expiry_date
                        };
                        $.ajax({
                            type: "POST",
                            url: window.location.protocol + '//' + window.location.host +'/api/addRegisteredDomain',
                            contentType: 'application/json',
                            data: JSON.stringify(body),
                            async: false,
                            success: function (data) {
                                console.log(data);
                            }
                        });

                        $.ajax({
                            type: "GET",
                            url:  window.location.protocol + '//' + window.location.host +'/api/Arecord?domain='+ domain_info.domain +'&ip1=' + domain_info.ip + '&staging='+ domain_info.staging,
                            async: false,
                            success:function(data){
                                console.log(data);
                            }
                        });

                        $.ajax({
                            type: "GET",
                            url: window.location.protocol + '//' + window.location.host +'/api/domainRegistered?domain='+ domain_info.domain +'&siteid=' + domain_info.siteid + '&staging='+ domain_info.staging +'&auth='+ domain_info.auth + '&agentid=' + domain_info.agentid,
                            async: false,
                            success:function(data){
                                console.log(data);
                            }
                        });
                        $('#successModal').modal('show');
                    }
                });
            }
        }).render('#paypal-button-container');
    });

})(jQuery);





